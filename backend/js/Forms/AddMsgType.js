var tableID = 'MsgTypeColsTable';

$("#msg").hide();
//$("#msg2").hide();
$("#addMsgType").validate();

$("input[name='key[]']").keyup(function() {
    $("#msg").hide();
    getJSon();
});
$("input[name='value[]']").keyup(function() {
    $("#msg").hide();
    getJSon();
});
$("[data-hide]").on("click", function() {
    $(this).closest("." + $(this).attr("data-hide")).hide();
});
//$("input[name='value[]']").keypress(function(event) {
//    if (event.which == 13) {
//        addRow();
//    }
//});

$("#addMsgType").click(function() {

    $("#addMsgTypeForm").validate();
    var $form = $("#addMsgTypeForm");
    // check if the input is valid
    if (!$form.valid())
    {
        if (!checkTableCols())
        {
            $("#msg").fadeIn();
            $("#msg").html("Please enter key-value pairs");
        }
        $('#type_name').focus();
        return false;
    }

    if (!checkTableCols())
    {
        $("#msg").fadeIn();
        $("#msg").html("Please enter key-value pairs");
        setFocus()
        return false;
    }

    var type_name = $("#type_name").val();
    var data = [];
    $("input[name='rows[]']").each(function() {
        var rownum = $(this).val();
        var key = $("#key" + rownum).val();
        var value = $("#value" + rownum).val();
        data.push({
            rownum: rownum,
            key: key,
            value: value
        });
    });
    $('#loading').html(urlLoading);
    $.post(urlAddMsgType, {data: data, type_name: type_name},
    function(data) {
        $('#loading').html("");
        var ret = JSON.parse(data);
        var flag = ret[0];
        var msg = ret[1];
        $("#msg2").fadeIn();
        $("#msg2").removeClass();
        if (flag)
        {
            $("#msg2").addClass("msg alert alert-info alert-dismissable");
            $("#msg2 span").html(msg);
            reset();
//            window.location = urlMsgTypes;
        } else
        {
            $("#msg2").addClass("msg alert alert-danger alert-dismissable");

        }
        $("#msg2 span").html(msg);
    });

});

$("#editMsgType").click(function() {
    $("#editMsgTypeForm").validate();
    var $form = $("#editMsgTypeForm");
    // check if the input is valid
    if (!$form.valid())
    {
        if (!checkTableCols())
        {
            $("#msg").fadeIn();
            $("#msg").html("Please enter key-value pairs");
        }
         $('#type_name').focus();
        return false;
    }

    if (!checkTableCols())
    {
        $("#msg").fadeIn();
        $("#msg").html("Please enter key-value pairs");
        setFocus()
        return false;
    }

    var type_name = $("#type_name").val();
    var type_id = $("#type_id").val();

    var data = [];
    $("input[name='rows[]']").each(function() {
        var rownum = $(this).val();
        var key = $("#key" + rownum).val();
        var value = $("#value" + rownum).val();
        var column_id = $("#rowNoDB" + rownum).val();
        data.push({
            rownum: rownum,
            column_id: column_id,
            key: key,
            value: value
        });
    });
    $('#loading').html(urlLoading);
    $.post(urlPostEditMsgType, {data: data, type_id: type_id, type_name: type_name},
    function(data) {
//        alert(data)
        $('#loading').html("");
        var ret = JSON.parse(data);
        var flag = ret[0];
        var msg = ret[1];

        $("#msg2").removeClass();
        if (flag)
        {
            location.reload();
//            var type_id = $("#type_id").val();
//            $.get(urlEditMsgType + "/" + type_id,
//                    function(data) {
//                        $("#MessageEdit").html(data);
//                    });

            $("#msg2").fadeIn();
            $("#msg2").addClass("msg alert alert-info alert-dismissable");
//            reset();
        } else
        {
            $("#msg2").fadeIn();
            $("#msg2").addClass("msg alert alert-danger alert-dismissable");

        }
        $("#msg2 span").html(msg);
    });

});

function addRow(rownum) {

    $("#msg").hide();

    var table = document.getElementById(tableID).getElementsByTagName('tbody')[0];
//    var table = document.getElementById(tableID);

    var rowCount1 = table.rows.length;

    if (rowCount1 > 0)
    {
        if (!checkTableCols())
        {
            $("#msg").fadeIn();
            $("#msg").html("Please enter key-value pairs");
            return false;
        }
    }
    if (rowCount1 < 10) {
        // limit the user from creating fields more than your limits
        var row = table.insertRow(rownum);
        var colCount = 4;//table.rows[0].cells.length;

        var rowCount = rownum + 1;

        for (var i = 0; i < colCount; i++) {
            var newcell = row.insertCell(i);
            if (i == 0)
            {
                newcell.innerHTML = '<p>' + rowCount + '</p> <input type="hidden" id="rowNo' + rowCount + '" name = rows[] value = "' + rowCount + '"  >';
                newcell.innerHTML = newcell.innerHTML + '<input type="hidden" id="rowNoDB' + rowCount + '" name = rowsDB[] value = "">';
            }
            else if (i == 1)
            {
                newcell.innerHTML = '<input type="text" id="key' + rowCount + '" name="key[]"  onkeyup="getJSon()" title="Please enter key ' + rowCount + '" class="form-control"/>';
            }
            else if (i == 2)
            {
                newcell.innerHTML = '<input type="text" id="value' + rowCount + '" name = "value[]"  onkeyup="getJSon()" title="Please enter value ' + rowCount + '" class="form-control"/>';
            }
            else if (i == 3)
            {
                newcell.innerHTML = '<button type="button" class="btn btn-primary" onClick="addRow(' + rowCount + ')" >+</button> ';
                newcell.innerHTML = newcell.innerHTML + '<button type="button" class="btn btn-primary" onClick="deleteRow(' + rowCount + ')" >-</button> ';
            }
        }
        reOrderTable();
        $('#key' + rowCount).focus();

    } else {
        $("#msg").fadeIn();
        $("#msg").html("Limit Is 10");
        return false;
    }
}

function deleteRow(row_id) {

    $("#msg").hide();
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    if (rowCount <= 2) {
        $("#msg").fadeIn();
        $("#msg").html("Cannot Remove All key-Value Pairs");
        return false;
    } else
    {
        /*---------------------------*/
        var row_del = table.rows[row_id];
        var col_first = row_del.cells[0];
        var db_id = col_first.children[2].value;

        if (db_id)
        {
            var type_id = $("#type_id").val();
            //delete from DB
            deleteRowFromDB(type_id, db_id);
//            if (!del)
//            {
//                return false;
//            }
        }
        /*---------------------------*/

        table.deleteRow(row_id);
        reOrderTable();
        getJSon();
    }
}
function reOrderTable()
{
    var table = document.getElementById(tableID);

    for (var i = 1, row; row = table.rows[i]; i++) {
        //iterate through rows
        //rows would be accessed using the "row" variable assigned in the for loop
        for (var j = 0, col; col = row.cells[j]; j++) {
            //iterate through columns
            //columns would be accessed using the "col" variable assigned in the for loop

            if (j == 0)
            {
                var old_value_db = col.children[2].value;
                col.innerHTML = '<p>' + i + '</p> <input type="hidden" id="rowNo' + i + '" name = rows[] value = "' + i + '"  >';
                col.innerHTML = col.innerHTML + '<input type="hidden" id="rowNoDB' + i + '" name = rowsDB[] value = "' + old_value_db + '">';
            }
            else if (j == 1)
            {
                var old_key = col.children[0].value;
                col.children[0].value = old_key;
                col.children[0].id = "key" + i;
                col.children[0].title = "Please enter key" + i;

//                    col.innerHTML = '<input type="text" id="key' + i + '" value = "' + old_key + '" name="key[]"  onkeyup="getJSon()"  title="Please Enter key ' + i + '" class="form-control"/>';
            }
            else if (j == 2)
            {
                var old_value = col.children[0].value;
                col.children[0].value = old_value;
                col.children[0].id = "value" + i;
                col.children[0].title = "Please enter value" + i;

                col.innerHTML = '<input type="text" id="value' + i + '" value = "' + old_value + '" name = "value[]"  onkeyup="getJSon()"  title="Please enter value ' + i + '" class="form-control"/>';
            }
            else if (j == 3)
            {
                col.innerHTML = '<button type="button" class="btn btn-primary" onClick="addRow(' + i + ')" >+</button> ';
                col.innerHTML = col.innerHTML + '<button type="button" class="btn btn-primary" onClick="deleteRow(' + i + ')" >-</button> ';
            }
        }
    }
}
function deleteRowFromDB(type_id, column_id)
{
    $.post(urlDelMsgTypeCol, {type_id: type_id, column_id: column_id}, {async: false},
    function(data) {
        var ret = JSON.parse(data);
        var flag = ret[0];
        var msg = ret[1];
        $("#msg2").fadeIn();
        $("#msg2").removeClass();

        if (flag)
        {
            $("#msg2").addClass("msg alert alert-info alert-dismissable");
        } else
        {
            $("#msg2").addClass("msg alert alert-danger alert-dismissable");
        }
        $("#msg2 span").html(msg);

    });
}
function checkTableCols()
{
    var table = document.getElementById(tableID);

    var rowCount = table.rows.length - 1;

    var count = 0;
    $("input[name='key[]']").each(function() {
        if ($.trim($(this).val()) != '') {
            count++;
        }
    });
//    alert(rowCount);
//    alert(count);
    if (rowCount > count)
    {
        return false;
    }
    return true;
}
function setFocus()
{
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length - 1;
    $('#key'+rowCount).focus();
}
function getJSon()
{
    $("#msg").hide();

    if (!checkTableCols())
    {
//        $("#msg").fadeIn();
//        $("#msg").html("Please enter key-value pairs");
        return false;
    }

    var data = [];
    $("input[name='rows[]']").each(function() {
        var rownum = $(this).val();
        var key = $("#key" + rownum).val();
        var value = $("#value" + rownum).val();
        data.push({
            rownum: rownum,
            key: key,
            value: value
        });
    });

    $.post(urlGetJson, {data: data},
    function(data) {
        $("#jsonPreview").html('<pre>' + data + '</pre>');
    });
}

function reset()
{
    document.getElementById("addMsgTypeForm").reset();
    $("#MsgTypeColsTable tbody").html("");
    addRow(0)
//    var table = document.getElementById(tableID).getElementsByTagName('tbody')[0];
//
//    var table = document.getElementById(tableID);
//    var rowCount = table.rows.length;

//    for (var i = 1, row; row = table.rows[i]; i++) {
//        table.deleteRow(i);
//    }

    $("#jsonPreview").html("");

}

