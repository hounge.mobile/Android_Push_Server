<?php

class Track {

    private $url;
    private $section;
    private $key;

    public function __construct() {
        $this->url = base_path() . "/installs/setup.ini";
        $this->section = 'setup';
        $this->key = 'track';
    }

    public function getTrack() {
        if (!file_exists($this->url)) {
            try {
                fopen($this->url, 'w');
            } catch (Exception $ex) {
                dd($ex->getMessage());
            }
        }
        $config = parse_ini_file($this->url, true);
        if (!isset($config[$this->section])) {
            $data = array(
                'track' => 0,
            );
            $this->writeTrack($data);
            return 0;
        }
        $setup = $config[$this->section];
        $track = $setup[$this->key] == '' ? 0 : $setup[$this->key];
        return $track;
    }

    public function writeTrack($assoc_arr) {
        if (!file_exists($this->url)) {
            try {
                fopen($this->url, 'w');
            } catch (Exception $ex) {
                dd($ex->getMessage());
            }
        }
        $content = "";
        $content .= "[" . $this->section . "]\n";
        foreach ($assoc_arr as $key => $elem) {
            if ($elem == "") {
                $content .= $key . " = '' \n";
            } else {
                $content .= $key . " = \"" . $elem . "\"\n";
            }
        }
        try {
            file_put_contents($this->url, $content);
            return true;
        } catch (Exception $ex) {
            dd($ex->getMessage());
            return false;
        }
    }

}
