<?php

class MailCustom {

    private static $instance = null;
    private static $info_mail;
    private static $site_url;
    private static $site_name;

    public static function get() {

        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        self::$info_mail = "info@reviapps.com";
        self::$site_url = Request::root();
        self::$site_name = 'Push notification ';
    }

    public function sendResetPasswordMail($data) {
        $to = $data['email'];
        $subject = self::$site_name . " password reset";
        $reset_url = self::$site_url . '/user/reset/' . $data["id"] . '/' . $data["reset_code"];

        $message = '
        <html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta name="viewport" content="width=device-width" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            </head>
            <body>
                <div style="background-color: rgb(245, 245, 245);;border-radius: 5px;
                    padding: 20px;width: 500px;
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size:12px; color:#000000;">                    
                    <p>Hi there ,</p>
                    <br/><p>Someone recently requested a password change for your ' . self::$site_name . '
                    account. If this was you, you can set a new password <a href=' . $reset_url . '>here</a></p>
                    <br/><p>If you don\'t want to change your password or didn\'t request this, kindly ignore this message.</p>                   
                    <br/><p>Thanks,<br/></p>
                </div>
            </body>
        </html>';
//        print_r($message);
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: Push Notification <' . self::$info_mail . '>';

        try {
            mail($to, $subject, $message, $headers);
            Log::info('Password reset mail sent successfully', ['to' => $to]);
            return array(true, 'success');
        } catch (Exception $ex) {
            Log::error('Failed to send password reset mail', ['Exception' => $ex->getMessage(), 'to' => $to]);
            return array(false, $ex->getMessage());
        }
    }

    public function sendmailToUserAfterFeedback($data) {
        $to = $data['email'];
        $subject = "Thank you for your feedback on " . self::$site_name;

        $message = '
        <html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta name="viewport" content="width=device-width" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            </head>
            <body>
                <div style="background-color: rgb(245, 245, 245);;border-radius: 5px;
                    padding: 20px;width: 500px;
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size:12px; color:#000000;">                    
                    <p>Dear  ' . $data['name'] . ' ,</p><br/>
                    <p>Thank you for your feedback.</p>
                    <p>Your message has been received and we will  get back to you as soon as possible.
                    <br/><p>We appreciate your participation in making Revi Push notification better.</p>
                    <br/><p>Thanks,<br/></p>
                    <p>Revi Team</p>
                </div>
            </body>
        </html>';
//        print_r($message);
//        dd("");
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: Push Notification <' . self::$info_mail . '>';

        try {
            mail($to, $subject, $message, $headers);
            Log::info('Feedback confirm mail sent successfull', ['to' => $to]);
            return array(true, 'success');
        } catch (Exception $ex) {
            Log::error('Failed to send feedback confirm mail', ['Exception' => $ex->getMessage(), 'to' => $to]);
            return array(false, $ex->getMessage());
        }
    }

    public function sendmailToAdminAfterFeedback($data) {
        $to = self::$info_mail;
        $subject = "New feedback from " . $data['name'];

        $message = '
        <html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
            <head>
                <meta name="viewport" content="width=device-width" />
                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            </head>
            <body>
                <div style="background-color: rgb(245, 245, 245);;border-radius: 5px;
                    padding: 20px;width: 500px;
                    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; font-size:12px; color:#000000;">                    
                    
                    <p>New feedback has been submitted by ' . $data['name'] . '</p>
                    <br/><p>' . $data['message'] . '</p>
                    <br/><p>Thanks,<br/></p>
                    <p>The Push Notification Team</p>
                </div>
            </body>
        </html>';
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= 'From: Push Notification <' . self::$info_mail . '>';

        try {
            mail($to, $subject, $message, $headers);
            Log::info('New feedback mail sent to admin', ['from' => $data['name'], 'to' => $to]);
            return array(true, 'success');
        } catch (Exception $ex) {
            Log::error('Failed to send feedback mail to admin', ['Exception' => $ex->getMessage(), 'from' => $data['name'], 'to' => $to]);
            return array(false, $ex->getMessage());
        }
    }

}
