<?php

class GCM {

    private static $instance = null;
    private static $google_api_key;

    public static function get() {

        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        $setting = Setting::first();
        if ($setting) {
            self::$google_api_key = $setting->api_key;
        }
    }

    public function send_notification($registatoin_ids, $message) {
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=' . self::$google_api_key,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === false) {
//            die('Curl failed: ' . curl_error($ch));
            Log::error('Curl failed:', ['context' => curl_error($ch)]);
            return false;
        }
        Log::info('Send state', ['context' => $result]);
        // Close connection
        curl_close($ch);
        return $result;
    }

}
