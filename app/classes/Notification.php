<?php

class Notification {

    public $success_count;
    public $fail_count;

    public function __construct() {
        $this->success_count = 0;
        $this->fail_count = 0;
    }

    public function init() {
        $this->success_count = 0;
        $this->fail_count = 0;
    }

    public function addMessageToDB($messageText, $type_id = 0) {
        try {
            $message = new Message();
            $message->message = $messageText;
            $message->type_id = $type_id;
            $message->save();
        } catch (Exception $e) {
            return array(false, -100);
        }
        return array(true, $message->id);
    }

    public function deleteMessage($message_id) {
        try {
            $message = Message::find($message_id);
            if ($message) {
                $message->delete();
                MessageState::where('message_id', '=', $message_id)->delete();
            }
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function sendNotification($message_id, $message, $users) {
        $this->init();
        try {
            foreach ($users as $user) {
                $user_id = $user->id;
                $registatoin_ids = array($user->gcm_regid);

                $json = GCM::get()->send_notification($registatoin_ids, $message);
                if (!$json) {
                    return -400; //Curl failed
                }
                
                $ret = json_decode($json, true);

                if ($ret['success']) {
                    $state = 1; //success
                    $this->success_count++;
                    $state_text = $ret['results'][0]['message_id'];
                } elseif ($ret['failure']) {
                    $state = 2; //failed
                    $this->fail_count++;
                    $state_text = $ret['results'][0]['error'];
                } else {
                    $state = 2; //failed
                    $this->fail_count++;
                    $state_text = strpos($json, 'Unauthorized') ? 'Unauthorized API key' : 'Undefined';
                }
//                $state_text = isset($ret['results'][0]['error']) ? $ret['results'][0]['error'] : $ret['results'][0]['message_id'];

                $message_state = new MessageState();
                $message_state->message_id = $message_id;
                $message_state->user_id = $user_id;
                $message_state->state = $state;
                $message_state->state_text = $state_text;
                $message_state->save();
            }
            return 101;
        } catch (Exception $e) {
            return -100;
        }
    }

}
