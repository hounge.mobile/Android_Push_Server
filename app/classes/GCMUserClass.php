<?php

class GCMUserClass {

    public function __construct() {
        
    }

    public function checkUserExistByRegID($gcm_regid, $id = 0) {
        if ($id != 0) { //update
            $user_count = GCMUser::
                    where('gcm_regid', '=', $gcm_regid)
                    ->where('id', '<>', $id)
                    ->count();
        } else {
            $user_count = GCMUser::where('gcm_regid', '=', $gcm_regid)->count();
        }

        if ($user_count > 0) {
            return true;
        }
        return false;
    }

    public function checkExistsByEmail($email, $id = 0) {
        if ($id != 0) { //update
            $count = GCMUser::
                    where('email', '=', $email)
                    ->where('id', '<>', $id)
                    ->count();
        } else { //add
            $count = GCMUser::where('email', '=', $email)->count();
        }
        if ($count > 0) {
            return true;
        }
        return false;
    }

}
