<?php

class TrackReq {

    private static $instance = null;

    public static function get() {

        if (self::$instance == null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private function __construct() {
        
    }

    public function sendTrack($domain_name) {
        // Set POST variables
        $url = 'http://reviapps.net/coreservices/track.php';
//        $url = 'localhost/core-services/track.php';

        $fields = array(
            'domain_name' => $domain_name
        );

        $headers = array(
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FAILONERROR, true);


        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);

        if ($result === false) {
            return array(false, curl_error($ch));
        }
        curl_close($ch);

        $res = json_decode($result);

        if ($res->code != '100') {
            return array(false, $res->message);
        }

        return array(true, $res->message);
    }

}
