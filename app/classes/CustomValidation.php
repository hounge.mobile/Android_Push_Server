<?php

class CustomValidation {

    public function verifyValue($value) {
        if (!isset($value) || strlen(trim($value)) <= 0) {
            return false;
        }
        return true;
    }

    public function verifyRequiredParams($req_params, $req_method) {
        $error = false;
        $error_fields = "";
        foreach ($req_params as $param) {
            if (!isset($req_method[$param]) || strlen(trim($req_method[$param])) <= 0) {
                $error = true;
                $error_fields .= $param . ', ';
            }
        }
        if ($error) {
            return array(-1, $error_fields);
        } else {
            return 0;
        }
    }

}
