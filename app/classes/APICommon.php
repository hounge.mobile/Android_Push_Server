<?php

class APICommon {

    public function getAPIoutput($index, $data = '', $txt = '') {
        $arr = array(
            "100" => array("code" => "100", "message" => "User added successfully"),
            "101" => array("code" => "100", "message" => "Notification Sent successfully"),
            "102" => array("code" => "100", "message" => "User deleted successfully"),
            "-100" => array("code" => "-100", "message" => "Database Error"),
            "-105" => array("code" => "-100", "message" => "Failed to add user"),
            "-106" => array("code" => "-100", "message" => "Failed to delete user"),
            "200" => array("code" => "200", "message" => "No Users Found to send message to them"),
            "-200" => array("code" => "-200", "message" => "User exists before"),
            "-201" => array("code" => "-200", "message" => "No user exists with this register id"),
            "-300" => array("code" => "-300", "message" => "Empty Json "),
            "-400" => array("code" => "-400", "message" => "Curl failed"),
            "-500" => array("code" => "-500", "message" => "Invalid JSON format "),
            "-600" => array("code" => "-600", "message" => "Required Parameters are Missing In JSON object : " . $txt),
        );
        return $arr[$index];
    }

}
