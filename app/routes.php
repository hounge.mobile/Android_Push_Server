<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */
Route::group(array('before' => 'track'), function() {
    Route::get('/', 'LoginController@getIndex');

    Route::Controller('login', 'LoginController');
    Route::Controller('login/login{first}', 'LoginController');
    Route::Controller('user', 'UserController');
    Route::Controller('message-types', 'MessageTypesController');
});
Route::group(array('before' => 'track|sentry.auth'), function() {
    Route::Controller('home', 'HomeController');
    Route::Controller('setting', 'SettingController');
    Route::Controller('gcm-users', 'GCMUserController');
    Route::Controller('send-notif', 'SendNotifController');
    Route::Controller('notif', 'NotifController');
    
    Route::Controller('feedback', 'FeedbackController');
});
Route::Controller('install', 'InstallController');
Route::resource('api-send-notif', 'APINotifController');
Route::resource('api-user', 'APIGCMUserController');

