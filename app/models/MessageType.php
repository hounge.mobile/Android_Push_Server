<?php

class MessageType extends Eloquent {

    protected $table = 'gcm_message_types';

    public function MsgTypeColumns() {
        return $this->hasMany('MessageTypeColums','type_id');
    }

}
