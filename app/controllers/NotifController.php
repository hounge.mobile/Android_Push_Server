<?php

class NotifController extends BaseController {

    public $count = 8;
    private $from_date;
    private $to_date;
    private $state;

    public function getIndex() {
        if (Request::ajax()) {
            $this->init();
            $status = Status::all();
            $all_messages = DB::table('gcm_messages_state')
                    ->leftJoin('gcm_messages', 'gcm_messages_state.message_id', '=', 'gcm_messages.id')
                    ->leftJoin('gcm_users', 'gcm_messages_state.user_id', '=', 'gcm_users.id')
                    ->leftJoin('gcm_status', 'gcm_messages_state.state', '=', 'gcm_status.id')
                    ->select(
                            'gcm_messages_state.id', 'gcm_messages_state.message_id', 'gcm_messages.message', 'gcm_messages_state.user_id', 'gcm_users.name as gcm_users_name', 'gcm_messages_state.state', 'gcm_status.name as gcm_status_name', 'gcm_messages_state.state_text', 'gcm_messages_state.created_at as gcm_messages_state_created_at'
                    )
                    ->whereRaw('gcm_messages_state.created_at >= "' . $this->from_date . '" and gcm_messages_state.created_at <= "' . $this->to_date . '" and gcm_messages_state.state like '.$this->state)
                    ->orderBy('gcm_messages_state.created_at', 'desc')
                    ->paginate($this->count);
            return Response::json(View::make('push.notif.reports', array('messages' => $all_messages,'status'=>$status))->render());
        }
        $all_messages = DB::table('gcm_messages_state')
                ->leftJoin('gcm_messages', 'gcm_messages_state.message_id', '=', 'gcm_messages.id')
                ->leftJoin('gcm_users', 'gcm_messages_state.user_id', '=', 'gcm_users.id')
                ->leftJoin('gcm_status', 'gcm_messages_state.state', '=', 'gcm_status.id')
                ->select(
                        'gcm_messages_state.id', 'gcm_messages_state.message_id', 'gcm_messages.message', 'gcm_messages_state.user_id', 'gcm_users.name as gcm_users_name', 'gcm_messages_state.state', 'gcm_status.name as gcm_status_name', 'gcm_messages_state.state_text', 'gcm_messages_state.created_at as gcm_messages_state_created_at'
                )
                ->orderBy('gcm_messages_state.created_at', 'desc')
                ->paginate($this->count);

        $status = Status::all();
        return View::make('push.notif.notif_state')
                        ->with('messages', $all_messages)
                        ->with('status', $status);
    }

    public function init() {
        if (Input::has('from_date')) {
            $this->from_date = Input::get('from_date');
        } else {
            $this->from_date = '0000-00-00';
        }
        if (Input::has('to_date')) {
            $this->to_date = date('Y-m-d', strtotime(Input::get('to_date') . ' + 1 days'));
            ; //Input::get('to_date');
        } else {
            $this->to_date = '9999-12-31';
        }
        if (Input::has('state')) {
            $this->state = Input::get('state');
        } else {
            $this->state = '"%%"';
        }
    }

}
