<?php

class MessageTypesController extends BaseController {

    public $validate_id_arr;
    public $count;

    public function __construct() {
        $this->validate_id_arr = array('id' => 'required|integer|min:1');
        $this->count = 20;

        $this->beforeFilter('sentry.auth', array('only' =>
            array('getIndex', 'getAddMsgType', 'getEditMsgType', 'postDelMsgType')));
    }

    public function getIndex() {
        if (Request::ajax()) {
            $types = MessageType::
                    orderBy('created_at', 'ASC')
                    ->paginate($this->count);
            return Response::json(View::make('push.message.message_type_filter', array('types' => $types))->render());
        }
        $types = MessageType::
                orderBy('created_at', 'ASC')
                ->paginate($this->count);
        return View::make('push.message.message_types')
                        ->with('types', $types);
    }

    public function postGetJson() {

        $rows = Input::get('data');
//        dd($rows);
        $arr = array();
        foreach ($rows as $row) {
            $arr[$row['key']] = $row['value'];
        }
//        print_r(json_encode($arr, JSON_PRETTY_PRINT));
        return Response::json(json_encode($arr, JSON_PRETTY_PRINT));
    }

    public function getAddMsgType() {
        return View::make('push.message.message_type_add');
    }

    public function postAddMsgType() {
        $type_name = Input::get('type_name');

        if ($this->checkTypeExists($type_name)) {
            return json_encode(array(false, "Notification type with the same name already exists before"));
        }
        $rows = Input::get('data');

        try {
            $msg_type = new MessageType();
            $msg_type->type_name = $type_name;
            $msg_type->save();
        } catch (Exception $e) {
            return json_encode(array(false, "Failed to add notification type"));
        }
        $type_id = $msg_type->id;
        try {
            foreach ($rows as $row) {
                $column = new MessageTypeColums();
                $column->type_id = $type_id;
                $column->key = $row['key'];
                $column->value = $row['value'];
                $column->save();
            }
//            Session::put('msg', 'Message Type and Columns Added Successfully');
            return json_encode(array(true, "Notification type and columns added successfully"));
        } catch (Exception $e) {

            MessageType::find($type_id)->delete();
            MessageTypeColums::where('type_id', '=', $type_id)->delete();

            if ($e->getCode() == 23000) {
                $msg = " : Keys Must be Unique";
            } else {
                $msg = "";
            }
            return json_encode(array(false, "Failed to add notification type columns" . $msg));
        }
    }

    public function getEditMsgType($id) {

        if (Request::ajax()) {
            $type = MessageType::find($id);
            $columns = MessageTypeColums::where('type_id', '=', $id)->orderBy('id', 'ASC')->get();
            return Response::json(View::make('push.message.edit', array('type' => $type, 'columns' => $columns))->render());
        }

        $validator = Validator::make(
                        array('id' => $id), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return Redirect::to('message-types');
        }

        $type = MessageType::find($id);
        if ($type) {
            $columns = MessageTypeColums::where('type_id', '=', $id)->orderBy('id', 'ASC')->get();

            return View::make('push.message.message_type_edit')
                            ->with('type', $type)
                            ->with('columns', $columns);
        } else {
            Log::warning('Invalid  notification type ID , GET', ['ID' => $id]);
            return Redirect::to('message-types');
        }
    }

    public function postEditMsgType() {
        $validator = Validator::make(
                        array('id' => Input::get('type_id')), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return json_encode(array(false, "Invalid notification type ID"));
        }

        $type_id = Input::get('type_id');
        $type_name = Input::get('type_name');
        $rows = Input::get('data');

        try {
            $msg_type = MessageType::find($type_id);
            $msg_type->type_name = $type_name;
            $msg_type->save();
        } catch (Exception $e) {
            return json_encode(array(false, "Failed to update notification type"));
        }
        try {
            foreach ($rows as $row) {

                if (isset($row['column_id']) && strlen(trim($row['column_id'])) > 0) {
                    $column_id = $row['column_id'];
                    $column = MessageTypeColums::find($column_id);
                } else {
                    $column = new MessageTypeColums();
                }

                $column->type_id = $type_id;
                $column->key = $row['key'];
                $column->value = $row['value'];
                $column->save();
            }
//            Session::put('msg', 'Message Type and Columns Updated Successfully');
            return json_encode(array(true, "Notification type and columns updated successfully"));
        } catch (Exception $e) {

            if ($e->getCode() == 23000) {
                $msg = " : Keys must be unique";
            } else {
                $msg = "";
            }
            return json_encode(array(false, "Failed to update notification type columns " . $msg));
        }
    }

    public function postDelMsgType() {
        $id = Input::get('id');
        $validator = Validator::make(
                        array('id' => $id), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return json_encode(array(false, "Invalid  notification type ID"));
//            return Redirect::to('message-types')
//                            ->with('msg', 'Invalid  notification type ID')
//                            ->with('state', '-1');
        }

        $type_id = $id;

//        $checkUsed = $this->checkMsgTypeUsed($type_id);
//        if ($checkUsed) {
//            return "Can't delete Notification Type.It's used";
//        }
        $type = MessageType::find($type_id);
        if (!$type) {
            return json_encode(array(false, "Type isn't found"));
        }
        try {
            MessageTypeColums::where('type_id', '=', $type_id)->delete();
            $type->delete();
            return json_encode(array(true, "Notification type deleted successfully"));
//            return Redirect::to('message-types')
//                            ->with('msg', 'Notification type deleted successfully');
        } catch (Exception $e) {
            return json_encode(array(false, "Can\'t delete notification type"));
//            return Redirect::to('message-types')
//                            ->with('msg', 'Can\'t delete notification type')
//                            ->with('state', '-1');
        }
    }

    public function postDelMsgTypeColumn() {
        $validator = Validator::make(
                        array('id' => Input::get('type_id')), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return "Invalid notification type ID";
        }

        $type_id = Input::get('type_id');
        $column_id = Input::get('column_id');

        try {
            $column = MessageTypeColums::find($column_id);
            $column->delete();
            return json_encode(array(true, "Notification type column deleted successfully"));
        } catch (Exception $e) {
            return json_encode(array(false, "Can't delete notification type column"));
        }
    }

    public function checkTypeExists($type_name) {
        $count = MessageType::where('type_name', '=', $type_name)->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function checkMsgTypeUsed($type_id) {
        $count = Message::where('type_id', '=', $type_id)->count();
        if ($count > 0) {
            return true;
        } else {
            return false;
        }
    }

}
