<?php

class GCMUserController extends BaseController {

    public $validate_id_arr;
    private $validator_user;
    private $validate_user_messages;
    public $count;
    private $user_obj;

    public function __construct() {
        $this->user_obj = new GCMUserClass();

        $this->validate_id_arr = array('id' => 'required|integer|min:1');
        $this->count = 20;
        $this->validator_user = array(
            'gcm_regid' => 'required|min:10',
            'name' => 'required|min:4',
            'email' => 'required|email'
        );
        $this->validate_user_messages = array(
            'gcm_regid.required' => 'Please enter register ID',
            'gcm_regid.min' => 'Register ID must be at least 10 characters',
            'name.required' => 'Please enter user name',
            'name.min' => 'User name at least 4 characters',
            'email.required' => 'Please enter email',
            'email.min' => 'Please enter valid email',
        );
    }

    public function getIndex() {
        if (Request::ajax()) {
            $value = Input::get('searchValue');
            $users = GCMUser::orderBy('created_at', 'DESC')
                    ->whereRaw('name like "%' . $value . '%" or gcm_regid like "%' . $value . '%" or email like "%' . $value . '%" or created_at like "%' . $value . '%"')
                    ->paginate($this->count);
            return Response::json(View::make('push.gcm_users.gcm_users_filter', array('users' => $users))->render());
        }
        $users = GCMUser::orderBy('created_at', 'DESC')->paginate($this->count);
        return View::make('push.gcm_users.gcm_users')->with('users', $users);
    }

    public function getAdd() {
        return View::make('push.gcm_users.gcm_users_add');
    }

    public function postAdd() {

        $validator = Validator::make(Input::all(), $this->validator_user, $this->validate_user_messages);
        if ($validator->fails()) {
            return Redirect::back()
                            ->withInput()
                            ->withErrors($validator)
            ;
        }

        $check_exists = $this->user_obj->checkUserExistByRegID(Input::get('gcm_regid'));

        if ($check_exists) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'This register ID already exists')
                            ->with('state', '-1');
        }

//        $check_exists2 = $this->user_obj->check_existsByEmail(Input::get('email'));
//
//        if ($check_exists2) {
//            return Redirect::back()
//                            ->withInput()
//                            ->with('msg', 'This email already registered before')
//                            ->with('state', '-1');
//        }

        try {
            $user = new GCMUser();
            $user->gcm_regid = Input::get('gcm_regid');
            $user->name = Input::get('name');
            $user->email = Input::get('email');
            $user->activated = 1;
            $user->save();

            return Redirect::to('gcm-users')
                            ->with('msg', 'User added successfully');
        } catch (Exception $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Failed to add user')
                            ->with('state', '-1');
        }
    }

    public function postActivate() {

        $validator = Validator::make(
                        array('id' => Input::get('id')), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return'Invalid ID';
        }

        try {
            $user = GCMUser::find(Input::get('id'));
            $user->activated = Input::get('active');
            $user->save();
        } catch (Exception $e) {
            return 'Error';
        }
    }

//    public function check_exists($email, $id = 0) {
//        if ($id != 0) {
//            $count = GCMUser::
//                    where('email', '=', $email)
//                    ->where('id', '<>', $id)
//                    ->count();
//        } else {
//            $count = GCMUser::where('email', '=', $email)->count();
//        }
//        if ($count > 0) {
//            return 1;
//        }
//        return 0;
//    }

    public function getEdit($id) {

        $validator = Validator::make(
                        array('id' => $id), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return Redirect::to('gcm-users');
            //return 'Invalid ID';
        }
        $user = GCMUser::find($id);
        if (!$user) {
            return Redirect::to('gcm-users');
        }
        return View::make('push.gcm_users.gcm_users_edit')
                        ->with('user', $user);
    }

    public function postEdit() {
        $id = Input::get('id');

        $validator = Validator::make(
                        array('id' => $id), $this->validate_id_arr
        );
        if ($validator->fails()) {
            return Redirect::to('gcm-users');
            //return 'Invalid ID';
        }
        $validator2 = Validator::make(Input::all(), $this->validator_user, $this->validate_user_messages);
        if ($validator2->fails()) {
            return Redirect::back()
                            ->withInput()
                            ->withErrors($validator2)
            ;
        }
        $check_exists = $this->user_obj->checkUserExistByRegID(Input::get('gcm_regid'), $id);

        if ($check_exists) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'This register ID already exists')
                            ->with('state', '-1');
        }

//        $check_exists = $this->user_obj->check_existsByEmail(Input::get('email'), $id);
//
//        if ($check_exists) {
//            return Redirect::back()
//                            ->withInput()
//                            ->with('msg', 'This email already registered before')
//                            ->with('state', '-1');
//        }

        try {
            $user = GCMUser::find($id);
            $user->gcm_regid = Input::get('gcm_regid');
            $user->name = Input::get('name');
            $user->email = Input::get('email');

            $user->save();

            return Redirect::to('gcm-users')
                            ->with('msg', 'User updated successfully');
        } catch (Exception $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Failed to update user')
                            ->with('state', '-1');
        }
    }

    public function postDelete() {
        $id = Input::get('id');
        $validator = Validator::make(
                        array('id' => $id), $this->validate_id_arr
        );
        if ($validator->fails()) {
//            return Redirect::to('gcm-users')
//                            ->with('msg', 'Invalid ID')
//                            ->with('state', '-1');
            return json_encode(array(false, "Invalid ID"));
        }

        $user = GCMUser::find($id);
        if (!$user) {
            return json_encode(array(false, "User isn't found"));
        }
        try {
            $user->delete();
//            return Redirect::to('gcm-users')
//                            ->with('msg', 'User deleted successfully');
            return json_encode(array(true, "User deleted successfully"));
        } catch (Exception $e) {
//            return Redirect::to('gcm-users')
//                            ->with('msg', 'Can\'t delete User')
//                            ->with('state', '-1');
            return json_encode(array(false, "Can\'t delete User"));
        }
    }

}
