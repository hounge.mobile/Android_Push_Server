<?php

class SendNotifController extends BaseController {

    public $validate_msg_arr;
    private $notification;
    private $message;
    private $message_arr;
    private $type_id;
    private $send_option;
    private $users;
    private $not_active_users_count;
    private $msg_to_displayed;
    private $count;

    public function __construct() {
        $this->validate_msg_arr = array('message' => 'required');
        $this->notification = new Notification();
    }

    public function getIndex() {
        $message_types = MessageType::all();
        $users = GCMUser::where('activated', '=', 1)->get();
        return View::make('push.notif.send_notif')
                        ->with('users', $users)
                        ->with('flag', '1')
                        ->with('message_types', $message_types);
    }

    public function getSend($id) {
        $message_types = MessageType::all();
        $users = GCMUser::where('activated', '=', 1)->get();
        return View::make('push.notif.send_notif')
                        ->with('users', $users)
                        ->with('flag', '2')
                        ->with('id', $id)
                        ->with('message_types', $message_types);
    }

    public function postGetNotifColumns() {
        $type_id = Input::get('type_id');

        $columns = MessageTypeColums::where('type_id', '=', $type_id)->orderBy('id', 'ASC')->get();
        return Response::json(View::make('push.notif.notif_columns', array('columns' => $columns))->render());
    }

    public function prepareMsgText() {

        $this->type_id = Input::get('type_id');
        $columns = MessageTypeColums::where('type_id', '=', $this->type_id)->orderBy('id', 'ASC')->get();

        $arr = array();

        foreach ($columns as $column) {
            $id = $column->id;
            $key = Input::get('key' . $id);
            $value = Input::get('value' . $id);
            $arr[$key] = $value;
        }
        $this->message_arr = $arr;
        $this->message = json_encode($arr, JSON_PRETTY_PRINT);
//        print_r($this->message_arr);
    }

    public function postSend() {
        $this->init();
        if ($this->count < 1) {
            Log::info('Send notification state', ['state' => 'No users found.']);
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'No users found.')
                            ->with('state', '-1')
            ;
        }

//        return $this->users;
        $this->prepareMsgText();

        $ret_add = $this->notification->addMessageToDB($this->message, $this->type_id);
        if (!$ret_add[0]) {
            Log::error('Send notification state', ['state' => 'Cant save message into database.']);
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Cant save message into database.')
                            ->with('state', '-1')
            ;
        }
        $message_id = $ret_add[1];
        $ret = $this->notification->sendNotification($message_id, $this->message_arr, $this->users);

        if ($ret == 101) {
            $this->setMesageToDisplayed();
            Log::info('Send notification state', ['state' => $this->msg_to_displayed]);
            return Redirect::to('send-notif')
                            ->with('msg', $this->msg_to_displayed)
                            ->with('state', '1')
            ;
        }

        $del = $this->notification->deleteMessage($message_id);
        if (!$del) {
            Log::error('Send notification state', ['state' => 'Errordd']);
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Errordd')
                            ->with('state', '-1')
            ;
        }

        if ($ret == -400) {
            $msg = "Curl failed";
            $state = '-1';
        } elseif ($ret == -100) {
            $msg = "Error";
            $state = '-1';
        }
        Log::error('Send notification state', ['state' => $msg]);
        return Redirect::to('send-notif')
                        ->withInput()
                        ->with('msg', $msg)
                        ->with('state', $state)
        ;
    }

    public function init() {
        $this->send_option = Input::get('sendOption');
        if ($this->send_option == 1) { //all
            
            $this->users = GCMUser::where('activated', '=', 1)->get();
            $this->not_active_users_count = GCMUser::where('activated', '=', 0)->count();
            
            if ($this->users) {
                $this->count = $this->users->count();
            } else {
                $this->count = 0;
            }
            
        } elseif ($this->send_option == 2) { //user
            
            $id = Input::get('users');
            $this->users = array(GCMUser::find($id));
            if (GCMUser::find($id)) {
                $this->count = 1;
            } else {
                $this->count = 0;
            }
        }
    }

    public function setMesageToDisplayed() {
        if ($this->send_option == 1) { //all
            $this->msg_to_displayed = "Notification sent successfully to " . $this->notification->success_count . " active users."
                    . "<br/> " . $this->notification->fail_count . " failed."
                    . "<br/> " . $this->not_active_users_count . " users are in active";
        } elseif ($this->send_option == 2) { //user
            if ($this->notification->success_count > 0) {
                $this->msg_to_displayed = "Notification sent successfully to user " . $this->users[0]->name;
            } else {
                $this->msg_to_displayed = "Failed to send notification to user " . $this->users[0]->name;
            }
        }
    }

}
