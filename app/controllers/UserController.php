<?php

class UserController extends BaseController {

    public $validate_id_arr;
    public $validate_user_arr;
    public $count;

    public function __construct() {
        $this->beforeFilter('sentry.auth', array('only' =>
            array('getChangePassword', 'postChangePassword')));

        $this->validate_id_arr = array('id' => 'required|integer|min:1');
        $this->validate_user_arr = array(
            'email' => 'required|email',
            'password' => 'required|min:4'
        );
        $this->count = 20;
    }

    public function getIndex() {
        return Redirect::to('send-notif');
    }

    public function getChangePassword() {
        return View::make('push.change-password');
    }

    public function postChangePassword() {

        $password = preg_replace('/\s+/', '', Input::get('password'));
        $old_password = preg_replace('/\s+/', '', Input::get('old_password'));
        $email = preg_replace('/\s+/', '', Input::get('email'));
        $password_confirmation = preg_replace('/\s+/', '', Input::get('password_confirmation'));

        $validation = array(
            'email' => 'required|email',
            'old_password' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        );
        $messages = array(
            'old_password.required' => 'The old password is required.',
            'password.required' => 'The password is required.',
            'password.min' => 'The password must be at least 6 characters(No spaces allowed).',
        );
        $validator = Validator::make(
                        array(
                    'email' => $email,
                    'old_password' => $old_password,
                    'password' => $password,
                    'password_confirmation' => $password_confirmation
                        ), $validation, $messages);

        if ($validator->fails()) {
            return Redirect::to('user/change-password')->withInput(Input::except('password'))->withErrors($validator);
        }
        try {
            // Find the current user 
            $credentials = array(
                'email' => $email,
                'password' => $old_password,
            );

            $user = Sentry::authenticate($credentials, false);

            // Get the password reset code
            $reset_code = $user->getResetPasswordCode();

            // Now you can send this code to your user via email for example.
            if ($user->checkResetPasswordCode($reset_code)) {
                // Attempt to reset the user password
                if ($user->attemptResetPassword($reset_code, Input::get('password'))) {
                    // Password reset passed
                    return Redirect::to('login/logout');
                } else {
                    // Password reset failed
                    return Redirect::to('user/change-password')
                                    ->withInput(Input::except('password'))
                                    ->with('msg', 'Password reset failed');
                }
            } else {
                // The provided password reset code is Invalid
            }
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->with('msg', 'Old password is wrong , try again');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::back()
                            ->with('msg', 'User was not found');
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::back()
                            ->with('msg', 'User is not activated');
        }
    }

    public function getResetPassword() {
        return View::make('push.user.reset-password');
    }

    public function postResetPassword() {
        /* ----------------validate data ------------------- */
        $email = preg_replace('/\s+/', '', Input::get('email'));

        $validation = array(
            'email' => 'required|email',
        );
        $validator = Validator::make(array('email' => $email), $validation);

        if ($validator->fails()) {
            return Redirect::to('user/reset-password')->withInput()->withErrors($validator);
        }
        /* --------check if user exists------------------- */
        try {
            // Find the user using the user email address
            $user = Sentry::findUserByLogin($email);
            // Get the password reset code
            $reset_code = $user->getResetPasswordCode();

//            $url = "http://domain.com/reset-password-form.php?id=" . $user['id'] . '&timestamp=' . $time . '&hash=' . $hash;
//            send_email($email, 'reset password email from xxx.com', ' Please click the following link to reset password' . $url);

            $data = array('id' => $user->id, 'email' => $email, 'reset_code' => $reset_code);
//            $this->sendResetPasswordMail($data);
            $ret = MailCustom::get()->sendResetPasswordMail($data);
            if ($ret[0]) {
                return Redirect::back()
                                ->with('msg', 'A password reset link was sent to your email.')
                                ->with('state', '1');
            } else {
                return Redirect::back()
                                ->withInput()
                                ->with('msg', 'failed to send email')
                                ->with('state', '-1');
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'User was not found.')
                            ->with('state', '-1');
        }
    }

    public function getReset($id, $rest_code) {
        // Find the user using the user id
        try {
            $user = Sentry::findUserById($id);
            return View::make('push.user.change-password')
                            ->with('user', $user)
                            ->with('rest_code', $rest_code);
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return 'User was not found.';
        }
    }

    public function postReset() {
        $password = preg_replace('/\s+/', '', Input::get('password'));
        $email = preg_replace('/\s+/', '', Input::get('email'));
        $password_confirmation = preg_replace('/\s+/', '', Input::get('password_confirmation'));

        $validation = array(
            'email' => 'required|email',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
        );
        $messages = array(
            'password.required' => 'The password is required.',
            'password.min' => 'The password must be at least 6 characters(No spaces allowed).',
        );
        $validator = Validator::make(
                        array(
                    'email' => $email,
                    'password' => $password,
                    'password_confirmation' => $password_confirmation
                        ), $validation, $messages);

        if ($validator->fails()) {
            return Redirect::back()->withInput(Input::except('password'))->withErrors($validator);
        }
        try {
            $id = Input::get('id');
            $reset_code = Input::get('reset_code');
            // Find the user using the user id
            $user = Sentry::findUserById($id);
            // Now you can send this code to your user via email for example.
            if ($user->checkResetPasswordCode($reset_code)) {
                // Attempt to reset the user password
                if ($user->attemptResetPassword($reset_code, $password)) {
                    // Password reset passed
                    return Redirect::to('login/logout');
                } else {
                    // Password reset failed
                    return Redirect::back()
                                    ->withInput(Input::except('password'))
                                    ->with('msg', 'Password reset failed');
                }
            } else {
                return Redirect::back()
                                ->withInput(Input::except('password'))
                                ->with('msg', 'The provided password reset code is Invalid.');
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::back()
                            ->with('msg', 'User was not found');
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::back()
                            ->with('msg', 'User is not activated');
        }
    }

//    public function sendResetPasswordMail($data) {
//        $site_url = Request::root();
//        $site_name = 'Push notification ';
//
//        $from = Common::get()->getInfoMail(); //'info@reviapps.com';
//        $to = $data['email'];
//        $subject = $site_name . " password reset";
//
//        $reset_url = $site_url . '/user/reset/' . $data["id"] . '/' . $data["reset_code"];
//
//        $message = '
//        <html lang="en-US" xmlns="http://www.w3.org/1999/xhtml">
//            <head>
//                <meta name="viewport" content="width=device-width" />
//                <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
//            </head>
//            <body>
//                <div class="panel panel-primary col-lg-6">
//                    <div class="panel-body col-lg-6">
//                        <p>Hi there ,</p>
//                        <p>Someone recently requested a password change for your ' . $site_name . '
//                        account. If this was you, you can set a new password</p>
//                        <p><a href=' . $reset_url . '>here</a></p>
//                        <p>If you dont want to change your password or didnt request this, just ignore and delete this message.</p>
//                       
//                        <h5>Thanks,<br/></h5>
//                        <h5>The Push Notification Team</h5>
//                    </div>
//                </div>
//            </body>
//        </html>';
////        dd($message);
//        $headers = 'MIME-Version: 1.0' . "\r\n";
//        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
//        $headers .= 'From: PTC <' . $from . '>';
//
//        mail($to, $subject, $message, $headers);
//    }
}
