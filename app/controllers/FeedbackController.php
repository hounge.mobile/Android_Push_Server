<?php

class FeedbackController extends BaseController {

    public function getIndex() {
        return View::make('push.feedback');
    }

    public function postSend() {
        $data_user = array('name' => Input::get('name'), 'email' => Input::get('email'));
        $ret_user = MailCustom::get()->sendmailToUserAfterFeedback($data_user);

        if (!$ret_user[0]) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'failed to send email')
                            ->with('state', '-1');
        }
        
        $data_admin = array('name' => Input::get('name'), 'email' => Input::get('email'),'message'=>Input::get('message'));
        $ret_admin = MailCustom::get()->sendmailToAdminAfterFeedback($data_admin);
        if (!$ret_admin[0]) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'failed to send email')
                            ->with('state', '-1');
        }

        return Redirect::back()
                        ->with('msg', 'Thank you for giving us your feedback.')
                        ->with('state', '1');
    }

}
