<?php

class APIGCMUserController extends \BaseController {

    private $validation;
    private $commom;
    private $user_obj;
    private $data;
    private $gcm_regid;
    private $name;
    private $email;
    private $created_at;
    private $validate_array;

    public function __construct() {
        $this->validation = new CustomValidation();
        $this->commom = new APICommon();
        $this->user_obj = new GCMUserClass();

        $this->created_at = date('Y-m-d H:i:s');
    }

    public function store() {

        $content = file_get_contents('php://input');
        $this->validate_array = array('gcm_regid', 'name', 'email');
        $check_input = $this->validateInput($content, 'add');
        if (!$check_input[0]) {
            return $check_input[1];
        }
        $this->init($this->data);

        $check_exist = $this->user_obj->checkUserExistByRegID($this->gcm_regid);
        if ($check_exist) {
            return $this->getOutput(-200);
        }
//        $check_exist2 = $this->user_obj->checkExistsByEmail($this->email);
//        if ($check_exist2) {
//            return $this->getOutput(-200);
//        }

        $user = new GCMUser();
        $user->gcm_regid = $this->gcm_regid;
        $user->name = $this->name;
        $user->email = $this->email;
        $user->activated = 1;

        $user->created_at = $this->created_at;

        try {
            $user->save();
            return $this->getOutput(100);
        } catch (Exception $e) {
            return $this->getOutput(-105);
        }
    }

    public function destroy() {
        $content = file_get_contents('php://input');
        $this->validate_array = array('gcm_regid');
        $check_input = $this->validateInput($content);
        if (!$check_input[0]) {
            return $check_input[1];
        }
       
        $this->init2($this->data);
       
        $check_exist = $this->user_obj->checkUserExistByRegID($this->gcm_regid);
        if (!$check_exist) {
            return $this->getOutput(-201); //no user exists with this reg_id
        }
         
        try {
            GCMUser::where('gcm_regid', '=', $this->gcm_regid)->delete();
            return $this->getOutput(102);
        } catch (Exception $e) {
            return $this->getOutput(-106);
        }
    }

    public function validateInput($content) {

        if (!$content) {
            $response = $this->commom->getAPIoutput(-300);
            return array(false, Response::json($response));
        }
        $data = json_decode($content, true);
        if (!$data) {
            $response = $this->commom->getAPIoutput(-500);
            return array(false, Response::json($response));
        }
        //verify data
        $check = $this->validation->verifyRequiredParams($this->validate_array, $data);
        if ($check <> 0) {
            $response = $this->commom->getAPIoutput(-600, '', $check[1]);
            return array(false, Response::json($response));
        }
        $this->setData($data);
        return array(true, 0);
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function init($data) {
        $this->gcm_regid = $data['gcm_regid'];
        $this->name = $data['name'];
        $this->email = $data['email'];
    }

    public function init2($data) {
        $this->gcm_regid = $data['gcm_regid'];
    }

//    public function checkUserExistBefore($gcm_regid) {
//        $user_count = GCMUser::where('gcm_regid', '=', $gcm_regid)->count();
//
//        if ($user_count > 0) {
//            return true;
//        }
//        return false;
//    }

    public function getOutput($index) {
        $response = $this->commom->getAPIoutput($index);
        return Response::json($response);
    }

}
