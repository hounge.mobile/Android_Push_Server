<?php

class APINotifController extends \BaseController {

    private $validation;
    private $commom;
    private $notification;
    private $data;
    private $message;
    private $message_id;
    private $link;
    private $img;
    private $category_id;

    public function __construct() {
        $this->validation = new CustomValidation();
        $this->commom = new APICommon();
        $this->notification = new Notification();
    }

    public function index() {
        
    }

    public function store() {
        $content = file_get_contents('php://input');

        $check_input = $this->validateInput($content);
        if (!$check_input[0]) {
            return $check_input[1];
        }
        $users = GCMUser::where('activated', '=', 1)->get();
        if ($users->count() < 1) {
            return $this->getOutput(200);
        }

        $this->init($this->data);

        $ret_add = $this->notification->addMessageToDB($this->message);
        if (!$ret_add[0]) {
            return $this->getOutput($ret_add[1]);
        }

        $this->message_id = $ret_add[1];

        $message_arr = array(
            "message" => $this->message
        );

        $ret = $this->notification->sendNotification($this->message_id, $message_arr, $users);
        if ($ret != 101) {
            $del = $this->notification->deleteMessage($this->message_id);
            if (!$del) {
                return $this->getOutput(-100);
            }
        }
        return $this->getOutput($ret);
    }

    
    public function validateInput($content) {

        if (!$content) {
            $response = $this->commom->getAPIoutput(-300);
            return array(false, Response::json($response));
        }
        $data = json_decode($content, true);
        if (!$data) {
            $response = $this->commom->getAPIoutput(-500);
            return array(false, Response::json($response));
        }
        //verify data
        $check = $this->validation->verifyRequiredParams(array('message'), $data);
        if ($check <> 0) {
            $response = $this->commom->getAPIoutput(-600, '', $check[1]);
            return array(false, Response::json($response));
        }
        $this->setData($data);
        return array(true, 0);
    }

    public function setData($data) {
        $this->data = $data;
    }

    public function init($data) {
        $this->message = $data['message'];
    }

    public function getOutput($index) {
        $response = $this->commom->getAPIoutput($index);
        return Response::json($response);
    }

}
