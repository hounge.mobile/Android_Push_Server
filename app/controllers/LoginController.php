<?php

class LoginController extends BaseController {

    public function getIndex() {
        Session::put('first', '0');
        if (Sentry::check()) {
//            return View::make('push.notif.send_notif');
            return Redirect::to('send-notif');
        } else {
            return View::make('push.login');
        }
    }

    public function getLogin() {
        Session::put('first', '1');
        if (Sentry::check()) {
            return Redirect::to('setting');
        } else {
            return View::make('push.login');
        }
    }

    public function postLogin() {

        $first = Session::get('first');
        if ($first == 1) {
            $route = 'setting';
        } else {
            $route = 'send-notif';
        }

        $validation = array(
            'email' => 'required|email',
            'password' => 'required|min:3'
        );
        $validator = Validator::make(
                        Input::all(), $validation);
        if ($validator->fails()) {
            return Redirect::to('/')->withInput(Input::except('password'))->withErrors($validator);
        }

        try {
            // Login credentials
            $credentials = array(
                'email' => Input::get('email'),
                'password' => Input::get('password'),
            );

            // Authenticate the user
            $user = Sentry::authenticate($credentials, false);

            // Log the user in
            Sentry::login($user, false);

            return Redirect::to($route);
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'Login field is required.');
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'Password field is required.');
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'Wrong password, try again.');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'User was not found.');
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'User is not activated.');
        } catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'User is suspended.');
        } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
            return Redirect::to('login')
                            ->withInput()
                            ->with('msg', 'User is banned.');
        }
    }

    public function getLogout() {
        Sentry::logout();
        return Redirect::to('login');
    }

}
