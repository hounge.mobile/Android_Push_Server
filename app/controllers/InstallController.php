<?php

class InstallController extends BaseController {

    private $track_obj;

    public function __construct() {
        $this->beforeFilter('track', array('only' =>
            array('getIndex', 'getAdmin')));
        $this->beforeFilter('databaseFile', array('only' =>
            array('getIndex')));
        $this->track_obj = new Track();
    }

    public function getIndex() {
        return View::make('install.database');
    }

    public function getAdmin() {
        return View::make('install.admin_account');
    }

    public function postAdmin() {

        $email = preg_replace('/\s+/', '', Input::get('email'));
        $password = preg_replace('/\s+/', '', Input::get('password'));

        $validation = array(
            'email' => 'required|email',
            'password' => 'required|min:6'
        );
        $messages = array(
            'password.required' => 'The password is required.',
            'password.min' => 'The password must be at least 6 characters(No spaces allowed).',
        );
        $validator = Validator::make(
                        array(
                    'email' => $email,
                    'password' => $password
                        ), $validation, $messages);
        if ($validator->fails()) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->withErrors($validator);
        }

        try {
            $user = Sentry::createUser(array(
                        'email' => $email,
                        'password' => $password,
                        'activated' => true,
            ));

            // Find the group using the group id
            $admin_group = Sentry::findGroupByName('Administrator');

            // Assign the group to the user
            $user->addGroup($admin_group);
//            return Redirect::to('install/complete');

            $credentials = array(
                'email' => $email,
                'password' => $password,
            );
            // Authenticate the user
            $auth = Sentry::authenticate($credentials, false);
            $data = array(
                'track' => 2,
            );
            $write = $this->track_obj->writeTrack($data);
            if ($write) {
                Log::info('Installation track wrote successfully');
            } else {
                Log::error('Failed to write installation track');
            }
//            TrackReq::get()->send_track($_SERVER['REQUEST_URI']);
            $ret = TrackReq::get()->sendTrack(Request::server('SERVER_NAME') . $_SERVER['REQUEST_URI']);
            if ($ret[0]) {
                Log::info('Installed successfully');
            } else {
                Log::error('Installation failed', ['context' => $ret[1]]);
            }

            return Redirect::to('setting');

//            return Redirect::to('login/login/1');
        } catch (Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->with('msg', 'Login field is required');
        } catch (Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->with('msg', 'Password field is required.');
        } catch (Cartalyst\Sentry\Users\UserExistsException $e) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->with('msg', 'User with this login already exists.');
        } catch (Cartalyst\Sentry\Groups\GroupNotFoundException $e) {
            return Redirect::back()
                            ->withInput(Input::except('password'))
                            ->with('msg', 'Group was not found.');
        } catch (Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Wrong password, try again.');
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'User was not found.');
        } catch (Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'User is not activated.');
        } catch (Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'User is suspended.');
        } catch (Cartalyst\Sentry\Throttling\UserBannedException $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'User is banned.');
        }
    }

    public function getComplete() {
        return View::make('install.install_complete');
    }

}
