<?php

class SettingController extends BaseController {

    public $validate_id_arr;
    public $validateImage;

    public function __construct() {
        $this->validate_id_arr = array('id' => 'required|integer|min:1');
        $this->validateImage = array(
            'image' => 'image|mimes:jpeg,jpg,png,gif,bmp'
        );
    }

    public function getIndex() {
        $setting = Setting::first();
        return View::make('push.setting')
                        ->with('setting', $setting);
    }

    public function postEdit() {
        if (Input::has('id')) {
            $validator = Validator::make(
                            array('id' => Input::get('id')), $this->validate_id_arr
            );
            if ($validator->fails()) {
                return Redirect::back()
                                ->withInput()
                                ->with('msg', 'Invalid ID')
                                ->with('state', '-1')
                ;
            }
        }
        if (Input::hasFile('logo')) {
            $mime = array('image/gif', 'image/jpeg', 'image/jpg', 'image/png', 'image/bmp');
            $uploaded_mime = Input::file('logo')->getMimeType();

            $file = array('image' => Input::file('logo'));
            $validator2 = Validator::make($file, $this->validateImage);

            if ($validator2->fails()) {
                return Redirect::back()
                                ->withInput()
                                ->with('msg', 'Logo must be an image of type: jpeg, jpg, png, gif,bmp')
                                ->with('state', '-1')
                ;
            }

            if (!in_array($uploaded_mime, $mime)) {
                return Redirect::back()
                                ->with('msg', 'Logo must be an image of type: jpeg, jpg, png, gif,bmp')
                                ->with('state', '-1')
                ;
            }
            list($width, $height) = getimagesize(Input::file('logo'));

            if ($width != '115' && $height != '50 ') {
                return Redirect::back()
                                ->with('msg', 'Logo dimensions must be 115 * 50 px')
                                ->with('state', '-1')
                ;
            }
            $logo = Input::file('logo');
            $destination_path = base_path() . '/backend/images/';
            $file_name = $logo->getClientOriginalName();
            Input::file('logo')->move($destination_path, $file_name);
        } else {
            $file_name = Input::get('logo_name');
        }

        $setting = Setting::first();
        if (!$setting) {
            $setting = new Setting();
        }
        try {
            $setting->api_key = Input::get('api_key');
            $setting->logo = $file_name;
            $setting->site_name = Input::get('site_name');
            $setting->save();

            return Redirect::to('setting')
                            ->with('msg', 'Setting save successfully')
                            ->with('state', '1')
            ;
        } catch (Exception $e) {
            return Redirect::back()
                            ->withInput()
                            ->with('msg', 'Failed to save settings.')
                            ->with('state', '-1')
            ;
        }
    }

}
