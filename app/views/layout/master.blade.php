<!DOCTYPE html>
<html class='no-js' lang='en'>

    <head>

        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <link rel="shortcut icon" href="<?php  echo asset('favicon.ico') ?>" type="image/x-icon">
        <link rel="icon" href="<?php  echo asset('favicon.ico') ?>" type="image/x-icon">
        <title>Push Notification Admin</title>

        <link href="<?php  echo asset('backend/assets/stylesheets/application-a07755f5.css') ?>" rel="stylesheet" type="text/css" />
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />


        <!-- SB Admin CSS - Include with every page -->
        <link href="<?php  echo asset('backend/css/sb-admin.css') ?>" rel="stylesheet">
        <link href="<?php  echo asset('backend/css/base/jquery.ui.all.css') ?>" rel="stylesheet">

        <!-- Core Scripts - Include with every page -->
        <script src="<?php  echo asset('backend/js/jquery-1.10.2.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/bootstrap.min.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/plugins/metisMenu/jquery.metisMenu.js') ?>"></script>

        <!--<script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js" type="text/javascript"></script>-->
        <!--<script src="<?php  echo asset('backend/assets/javascripts/application-985b892b.js') ?>" type="text/javascript"></script>-->


        <script src="<?php  echo asset('backend/js/jquery.ui.core.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/jquery.ui.widget.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/jquery.ui.datepicker.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/jquery-ui-timepicker-addon.js') ?>"></script>

        <script src="<?php  echo asset('backend/js/dist/jquery.validate.js') ?>"></script>

        <!--<script src="<?php  echo asset('backend/js/Forms/user.js') ?>"></script>-->


        <!-- SB Admin Scripts - Include with every page -->
        <script src="<?php  echo asset('backend/js/sb-admin.js') ?>"></script>
    </head>

    <body class='main page'>
        <div class='navbar navbar-default' id='navbar'>
            <a class="navbar-brand" href="<?php  echo URL::to('send-notif') ?>">
                <?php
                $url = isset($setting_share) ? isset($setting_share->logo) && !empty($setting_share->logo) ? $setting_share->logo : 'default-logo.png' : 'default-logo.png';
                ?>

                <img src="<?php  echo asset('backend/images/'.$url) ?>" style="width: 115px;height: 50px;" />
                <!--Push Notification Admin-->
                <?php echo isset($setting_share) ? isset($setting_share->site_name) ? $setting_share->site_name : '' : ''; ?>

            </a>
            <?php
            if (Sentry::check()) {
                ?>
                <ul class='nav navbar-nav pull-right'>
                    <li>
                        <a href='<?php  echo URL::to('setting') ?>'>
                            <i class='icon-cog'></i>
                            Settings
                        </a>
                    </li>
                    <li class='dropdown user'>
                        <a class='dropdown-toggle' data-toggle='dropdown' href='#'>
                            <i class='icon-user'></i>
                            <strong>Admin</strong>
                            <img class="img-rounded" src="http://placehold.it/20x20/ccc/777" />
                            <b class='caret'></b>
                        </a>
                        <ul class='dropdown-menu'>
                            <li>
                                <a href='<?php  echo URL::to('user/change-password') ?>'>Change Password</a>
                            </li>
                            <li class='divider'></li>
                            <li>
                                <a href="<?php  echo URL::to('login/logout') ?>">Sign out</a>
                            </li>
                        </ul>
                    </li>
                </ul>
            <?php } ?>
        </div>
        <?php $route = Route::getCurrentRoute()->getPath() ?>
        <div id='wrapper'>
            <section id='sidebar'>
                <i class='icon-align-justify icon-large' id='toggle'></i>
                <ul id='dock'>
                    <li class='launcher <?php echo strpos($route, 'send-notif') !== false ? 'active' : '' ?>'>
                        <i class='icon-dashboard'></i>
                        <a href="<?php  echo URL::to('send-notif') ?>">Send</a>
                    </li>

                    <li class="launcher <?php echo strpos($route, 'message-types') !== false ? 'active' : '' ?>">
                        <i class='icon-file-text-alt'></i>
                        <a href="<?php  echo URL::to('message-types') ?>">Types</a>
                    </li>

                    <li class="launcher <?php echo strpos($route, 'gcm-users') !== false ? 'active' : '' ?>">
                        <i class='icon-table'></i>
                        <a href="<?php  echo URL::to('gcm-users') ?>">Users</a>
                    </li>

                    <li class="launcher <?php echo strpos($route, 'notif') !== false && $route == 'notif' ? 'active' : '' ?>">
                        <i class='icon-flag'></i>
                        <a href="<?php  echo URL::to('notif') ?>">Reports</a>
                    </li>

                    <li class="launcher <?php echo strpos($route, 'setting') !== false ? 'active' : '' ?>">
                        <i class='icon-dashboard'></i>
                        <a href="<?php  echo URL::to('setting') ?>">Settings</a>
                    </li>

                    <li class="launcher <?php echo strpos($route, 'feedback') !== false ? 'active' : '' ?>">
                        <i class='icon-bug'></i>
                        <a href="<?php  echo URL::to('feedback') ?>">Feedback</a>
                    </li>
                </ul>
            </section>
            @yield('content')

        </div>
        <div id="footer">
            <div class="footerContent">
                Copyright © ReviApps 2014. All Rights Reserved
            </div>
        </div>
    </body>

</html>
