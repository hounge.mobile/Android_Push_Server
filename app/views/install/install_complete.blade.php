@extends('install.install_master');

@section('content')
<div class="row">
    <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="text-center text-info">Install Revi Notifications</h3>
            </div>

            <div class="panel-body">
                <div class = "msg span7 alert alert-info">            
                    Installation completed successfully
                </div>
                <div class="table-responsive">  
                    <a href="<?php  echo URL::to('login/login/1') ?>">Let's start</a>
                </div>
            </div>
        </div>
    </div>
</div>
@stop