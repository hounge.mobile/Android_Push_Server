<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <link rel="shortcut icon" href="<?php  echo asset('favicon.ico') ?>" type="image/x-icon" />
        <link rel="icon" href="<?php  echo asset('favicon.ico') ?>" type="image/x-icon" />
        <title>Push Notification - Installation</title>

        <link href="<?php  echo asset('backend/assets/stylesheets/application-a07755f5.css') ?>" rel="stylesheet" type="text/css" />
        <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- SB Admin CSS - Include with every page -->
        <link href="<?php  echo asset('backend/css/sb-admin.css') ?>" rel="stylesheet">

        <script src="<?php  echo asset('backend/js/jquery-1.10.2.js') ?>"></script>
        <script src="<?php  echo asset('backend/js/dist/jquery.validate.js') ?>"></script>

        <script src="//cdnjs.cloudflare.com/ajax/libs/modernizr/2.6.2/modernizr.min.js" type="text/javascript"></script>
        <script src="<?php  echo asset('backend/assets/assets/javascripts/application-985b892b.js') ?>" type="text/javascript"></script>

    </head>
    <body class="login"  >
        <div class='wrapper' style="width: 500px">
            @yield('content')
        </div>
    </body>
</html>