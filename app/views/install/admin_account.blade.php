@extends('install.install_master');

@section('content')
<div class='row'>
    <div class='col-lg-12'>
        <div class='brand text-center'>
            <h1 style="font-size: 30px">
                <div class=''>
                    <img src="<?php  echo asset('backend/images/default-logo.png') ?>" />
                </div>
                <br/>
                Install admin account
            </h1>
        </div>
    </div>
</div>
<div class="row">
    <div class='col-lg-12'>
        <?php
        $msg = Session::get('msg');
        ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg span7 alert alert-danger">            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <div class="table-responsive">  
            <div  >               
                <?php  echo  Form::open(array('url' => URL::to('install/admin'),'id'=>"installAdmin"))  ?>
                <div class="form-group">
                    <?php  echo Form::label('email', 'Admin email') ?>
                    <?php  echo Form::text('email','' ,array('class'=>'form-control','placeholder'=>'Admin email')) ?>
                    <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
                </div>
                <div class="form-group">
                    <?php  echo Form::label('password', 'Admin password') ?>
                    <input type="password" id="password" placeholder="Password" name="password" class="form-control" />
                    <p class="alert-danger"><?php  echo $errors->first('password') ?></p>
                </div>
                <div class="form-group">
                    <?php  echo Form::label('confirm_password', 'Confirm password') ?>                              
                    <input type="password" id="password_confirmation" placeholder="Confirm password" name="password_confirmation" class="form-control" />
                    <p class="alert-danger"><?php  echo $errors->first('password_confirmation') ?></p>
                </div>
                <?php  echo Form::submit('Submit',array('class'=>'btn btn-primary')) ?>
                <?php  echo  Form::close()  ?>
            </div>       
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#installAdmin").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                },
                password: {
                    required: "The password is required",
                    minlength: "The password must be at least 6 characters"
                },
                password_confirmation: {
                    required: 'Please confirm password',
                    minlength: 'Confirm password must be at least 6 characters',
                    equalTo: 'Confirm password didnt match password'
                }
            }
        });
    });

</script>
@stop