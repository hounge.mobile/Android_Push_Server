@extends('install.install_master')

@section('content')
<div class='row'>
    <div class='col-lg-12'>
        <div class='brand text-center'>
            <h1 style="font-size: 30px">
                <div class=''>
                    <img src="<?php  echo asset('backend/images/default-logo.png') ?>" />
                </div>
                <br/>
                Install database
            </h1>
        </div>
    </div>
</div>
<div class="row">
    <div class='col-lg-12'>
        <?php
        $msg = Session::get('msg');
        $state = Session::get('state');
        ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg span7 alert alert-<?php  echo isset($msg) ? $state==1 ? 'info' : 'danger' : 'danger' ?>">            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <div class="table-responsive">  
            <div  >               
                <?php  echo  Form::open(array('url' => asset('installs/bigdump.php'),'id'=>"installForm"))  ?>
                <input type="hidden" id="siteURL" name="siteURL" value="<?php  echo asset('/') ?>" />
                <div class="form-group">
                    <?php  echo Form::label('database_name', 'Database name') ?>
                    <?php  echo Form::text('database_name','' ,array('class'=>'form-control','placeholder'=>'Database name')) ?>
                    <p class="alert-danger"><?php  echo $errors->first('database_name') ?></p>
                </div>
                <div class="form-group">
                    <?php  echo Form::label('user_name', 'User name') ?>
                    <?php  echo Form::text('user_name','' ,array('class'=>'form-control','placeholder'=>'User name')) ?>
                    <p class="alert-danger"><?php  echo $errors->first('user_name') ?></p>
                </div>
                <div class="form-group">
                    <?php  echo Form::label('password', 'Password') ?>
                    <!--<?php  echo Form::text('password','' ,array('class'=>'form-control','placeholder'=>'Password')) ?>-->
                    <input type="password" id="password" placeholder="Password" name="password" class="form-control" />
                    <p class="alert-danger"><?php  echo $errors->first('password') ?></p>
                </div>

                <div class="col-md-4 pull-left" style="padding: 0px" >
                    <?php  echo Form::button('Install',array('class'=>'btn btn-primary','onclick'=>'installDB()')) ?>             
                </div>
                <div class="col-md-4 pull-left">
                    <div id="loading">

                    </div>
                </div>

                <?php  echo  Form::close()  ?>

            </div>       
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#installForm").validate({
            rules: {
                database_name: {
                    required: true
                },
                user_name: {
                    required: true
                }
            },
            messages: {
                database_name: {
                    required: "Please enter database name"
                },
                user_name: {
                    required: "Please enter user name"
                }
            }
        });


    });
    var url = "Processing... <img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>>";
    function installDB()
    {
        $("#installForm").validate();
        var $form = $("#installForm");
        // check if the input is valid
        if (!$form.valid())
        {
            return false;
        }
        $('#loading').html(url);
        $("#installForm").submit();
//        $('#loading').html("");
    }
    $("#installForm").bind('ajax:complete', function() {
        $('#loading').html("");
    });
</script>
@stop