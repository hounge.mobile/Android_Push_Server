@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Send notification</li>
    </ul>
</section>
<?php
$msg = Session::get('msg');
$state = Session::get('state');
?>
<div id='content'>
    <div class="col-md-8">
        <div id = "msg" style="<?php echo isset($msg) ? '' : 'display:none' ?>" class = "msg col-md-9 alert alert-dismissable alert-<?php echo isset($msg) ? $state == 1 ? 'info' : 'danger' : 'danger' ?>">            
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <?php echo isset($msg) ? $msg : '' ?>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-8">
        <div class="col-md-9" style="padding: 0px">
            <?php echo Form::open(array('url' => URL::to('send-notif/send'), 'id' => "sendMsgForm")) ?>

            <div class="form-group">
                <label>Notification type</label>
                <select class="form-control" name="type_id" id="type_id" required title="Please select type">
                    <option value=""> -- Select type --</option>
                    <?php foreach ($message_types as $message_type) { ?>
                        <option value="<?php echo $message_type->id ?>" <?php echo Input::old('type_id') == $message_type->id ? 'selected' : '' ?> ><?php echo $message_type->type_name ?></option>
                    <?php } ?>
                </select>
                <p class="alert-danger"><?php echo $errors->first('type_id') ?></p>
            </div>

            <div id="NotifColumns">

            </div>
            <div id="loading">

            </div>
            <b/>
            <b/>
            <div class="form-group form-inline">
                <?php echo Form::button('Send', array('class' => 'btn btn-primary', 'onclick' => 'sendNotif()')) ?>

                <label style="margin-left: 10px;"> To : </label>
                <div class="radio" >
                    <label>
                        <input type="radio" <?php echo Input::old('sendOption') == 1 ? 'checked' : $flag == 1 ? 'checked' : '' ?> name="sendOption" id="optionsRadios1" value="1" checked><span style="padding: 0px 5px;">All Users</span>
                    </label>
                </div>
                <div class="radio">
                    <label>
                        <input type="radio" <?php echo Input::old('sendOption') == 2 ? 'checked' : $flag == 2 ? 'checked' : '' ?> name="sendOption" id="optionsRadios2" value="2"><span style="padding: 0px 5px;">User</span>
                    </label>
                </div>
                <div id="Users" style="display: inline">
                    @include('push.gcm_users.gcm_users_list')
                </div>

            </div>

            <?php echo Form::close() ?>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#sendMsgForm").validate();
    //    $("#sendMsgForm").validate({
    //        rules: {
    //            message: {
    //                required: true,
    //                minlength: 10
    //            }
    //        },
    //        messages: {
    //            message: {
    //                required: "Please enter message text",
    //                minlength: "Message should be at least 10 characters"
    //            }
    //        }
    //    });
    var urlGetColumns = "<?php echo URL::to('send-notif/get-notif-columns') ?>";
    var url = "<img src=<?php echo URL::asset('backend/images/Chasing_blocks.gif') ?>> Processing...";

    var types_count = $("#type_id option").length;

    if (types_count == 2)
    {
        $("#type_id").prop('selectedIndex', 1);
        var value = $('#type_id option:selected').val();
        getNotifColumns(value);
    }
    $('#type_id').on('change', function() {
        getNotifColumns(this.value);
    });

    function getNotifColumns(type_id)
    {
        $('#loading').html(url);
        $.post(urlGetColumns, {type_id: type_id},
        function(data) {
            $('#loading').html("");
            $("#NotifColumns").html(data);
        });
    }

    var val = $('input[name=sendOption]:checked').val();
    visibilty(val)

    $(function() {
        $("input[type=radio][name=sendOption]").change(function() {
            visibilty(this.value)
        });
    });

    function visibilty(val)
    {
        if (val == '1') {
            $("#Users").hide();
        } else if (val == '2') {
            $("#Users").show();
            $("#users").attr("required", "true");
        }
    }
    function sendNotif()
    {
        $("#sendMsgForm").validate();
        var $form = $("#sendMsgForm");
        // check if the input is valid
        if (!$form.valid())
        {
            return false;
        }
        $('#loading').html(url);
        $("#sendMsgForm").submit();
    }

</script>
@stop