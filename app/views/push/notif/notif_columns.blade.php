
<?php foreach ($columns as $column) { ?>
    <div class="form-group">
        <!--<?php echo Form::text('email', 'example@gmail.com') ?>-->
        <?php echo Form::label('keylabel' . $column->id, $column->key) ?>
        <?php echo Form::hidden('key' . $column->id, $column->key, array('id' => 'key' . $column->id)) ?>
        <?php echo Form::textarea('value' . $column->id, $column->value, array('id' => 'value' . $column->id, 'class' => 'form-control', 'required' => 'true', 'title' => 'Please enter value for key ' . $column->key, 'placeholder' => 'Value For Key ' . $column->key)) ?>
        <p class="alert-danger"><?php echo $errors->first('key' . $column->id) ?></p>
    </div>
    <?php
    }    