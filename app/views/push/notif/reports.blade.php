<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th>Message</th>
            <th>User name</th>
            <th>Message state</th>
            <th>Reason</th>
            <th>Date</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($messages as $message) { ?>
            <tr>
                <td><?php echo substr($message->message, 0, 100) ?></td>
                <td><?php echo $message->gcm_users_name ?></td>
                <td><?php echo $message->gcm_status_name ?></td>
                <td><?php echo $message->state_text ?></td>
                <td><?php echo $message->gcm_messages_state_created_at ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php echo $messages->links() ?>
