@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Reports</li>
    </ul>
</section>
<div id='content'>
    <div class="panel panel-default grid">
        <div class='panel-body filters' >
            <div class='col-md-12'>
                <?php echo Form::open(array('url' => URL::to('setting/edit'), 'id' => "editSettingForm")) ?>
                <div class="form-inline">
                    <div class="form-group">
                        <?php echo Form::label('from_date', 'From:') ?>
                        <?php echo Form::text('from_date', '', array('class' => 'form-control', 'placeholder' => 'From date', 'id' => 'searchFromDate')) ?>                                                       
                    </div>

                    <div class="form-group">
                        <?php echo Form::label('to_date', 'To:') ?>
                        <?php echo Form::text('to_date', '', array('class' => 'form-control', 'placeholder' => 'To date', 'id' => 'searchToDate')) ?>                                                      
                    </div>
                    <div class="form-group">
                        <select class="form-control" id="searchState" onchange="searchReports(1)">
                            <option value="">-- All --</option>
                            <?php foreach ($status as $state) {
                                ?>
                                <option value="<?php echo $state->id ?>"><?php echo $state->name ?></option>
                                <?php
                            }
                            ?>
                        </select>
                    </div>
                    <div class="form-group"  >
                        <input type="button" class="btn btn-success btn-sm" onclick="searchReports(1)" value="Go" />
                    </div>
                </div>
                <?php echo Form::close() ?>
            </div>
        </div>
        <div id="messagesList">
            @include('push.notif.reports')
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#searchFromDate").datepicker({
        showButtonPanel: true,
        dateFormat: "yy-mm-dd",
        onClose: function(dateText, inst) {
            if ($('#searchFromDate').val() != '') {
                var testStartDate = $('#searchFromDate').datetimepicker('getDate');
                var testEndDate = $('#searchToDate').datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    $('#searchToDate').datetimepicker('setDate', testStartDate);
            }
            else {
                $('#searchToDate').val(dateText);
            }
        },
        onSelect: function(selectedDateTime) {
            $('#searchToDate').datetimepicker('option', 'minDate', $('#searchFromDate').datetimepicker('getDate'));
        }
    });
    $("#searchToDate").datepicker({
        showButtonPanel: true,
        dateFormat: "yy-mm-dd",
        onClose: function(dateText, inst) {
            if ($('#searchFromDate').val() != '') {
                var testStartDate = $('#searchFromDate').datetimepicker('getDate');
                var testEndDate = $('#searchToDate').datetimepicker('getDate');
                if (testStartDate > testEndDate)
                    $('#searchFromDate').datetimepicker('setDate', testEndDate);
            }
            else {
                $('#searchFromDate').val(dateText);
            }
        },
        onSelect: function(selectedDateTime) {
            $('#searchFromDate').datetimepicker('option', 'maxDate', $('#searchToDate').datetimepicker('getDate'));
        }
    });

    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                searchReports(page);
            }
        }
    });
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(e) {
            searchReports($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    });
    function searchReports(page)
    {
        var from_date = $("#searchFromDate").val();
        var to_date = $("#searchToDate").val();
        var state = $("#searchState").val();

        $.get("?page=" + page, {from_date: from_date, to_date: to_date, state: state, dataType: 'json'},
        function(data) {
            $("#messagesList").html(data);
            location.hash = page;
        });
    }

</script>
@stop