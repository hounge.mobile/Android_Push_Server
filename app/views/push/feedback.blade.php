@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Feedback</li>
    </ul>
</section>
<?php
$msg = Session::get('msg');
$state = Session::get('state');
?>
<div id='content'>
    <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg col-lg-6 alert alert-dismissable alert-<?php  echo isset($msg) ? $state==1 ? 'info' : 'danger' : 'danger' ?>">            
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php  echo isset($msg) ? $msg : '' ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-6" style="padding-left: 0px;" >  
        <div class="form-group">
            <p>We would like to hear your feedback and comments</p>
        </div>
        <?php  echo  Form::open(array('url' => URL::to('feedback/send'),'id'=>"sendFeedbackForm", 'files' => true))  ?>
        <div class="form-group">
            <?php  echo Form::text('name', '' ,array('class'=>'form-control','placeholder'=>'Name')) ?>
            <p class="alert-danger"><?php  echo $errors->first('name') ?></p>
        </div>
        <!--<hr/>-->
        <div class="form-group">
            <?php  echo Form::text('email', '' ,array('class'=>'form-control','placeholder'=>'Email')) ?>
            <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::textarea('message', '' ,array('class'=>'form-control','placeholder'=>'Message','style'=>'height:200px')) ?>
            <p class="alert-danger"><?php  echo $errors->first('message') ?></p>
        </div>
        <?php  echo Form::submit('Send',array('class'=>'btn btn-primary')) ?>
        <?php  echo  Form::close()  ?>
    </div>      
</div>
<script type="text/javascript" >

    $("#sendFeedbackForm").validate({
        rules: {
            name: {
                required: true,
                minlength: 3
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true,
                minlength: 6
            }
        },
        messages: {
            name: {
                required: "Please enter your name",
                minlength: "Name is at least 3 chars"
            },
            email: {
                required: "Please enter your email",
                email: "Please enter valid email"
            },
            message: {
                required: "Please enter your message",
                minlength: "Message is at least 6 chars"
            }
        }
    });
</script>
@stop