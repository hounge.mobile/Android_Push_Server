@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Change password</li>
    </ul>
</section>
<div id="content" >
    <div class="col-md-6">
        <?php $msg = Session::get('msg'); ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg span7 alert alert-danger">            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>

        <?php
        $user = Sentry::getUser();
        ?>
        <?php  echo  Form::open(array('url' => URL::to('user/change-password'),'id'=>'ChangePassForm'))  ?>
        <div class="form-group">
            <?php  echo Form::label('email', 'E-mail') ?>
            <?php  echo Form::text('email',$user->email,array('class'=>'form-control','placeholder'=>'E-mail','readonly'=>'true')) ?>                               
            <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::label('old_password', 'Old password') ?> 
            <input type="password" id="old_password" name="old_password" class="form-control" />
            <p class="alert-danger"><?php  echo $errors->first('old_password') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::label('password', 'New password') ?> 
            <input type="password" id="password" name="password" class="form-control" />
            <p class="alert-danger"><?php  echo $errors->first('password') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::label('confirm_password', 'Confirm password') ?>                              
            <input type="password" id="password_confirmation" name="password_confirmation" class="form-control" />
            <p class="alert-danger"><?php  echo $errors->first('password_confirmation') ?></p>
        </div>
        <?php  echo Form::submit('Change password',array('class'=>'btn btn-success')) ?>
        <?php  echo  Form::close()  ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#ChangePassForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                old_password: {
                    required: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: 'Please eEnter email',
                    email: 'Please enter valid email'
                },
                old_password: {
                    required: 'Old password is required'
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password is at least 6 chars"
                },
                password_confirmation: {
                    required: 'Please confirm password',
                    minlength: 'Confirm password must be at least 6 characters',
                    equalTo: 'Confirm password didnt match password'
                }
            }
        });
    });
</script>
@stop