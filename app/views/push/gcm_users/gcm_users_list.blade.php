<select name='users' title="Please select user" id='users' class="small-control" style="width: 255px" >
    <option value=''>-- Select user --</option>
    <?php foreach ($users as $user) { ?>
        <option value='<?php echo $user->id ?>' <?php echo Input::old('users') == $user->id ? 'selected' : isset($id) && $id == $user->id ? 'selected' : '' ?> ><?php echo $user->name ?> - <?php echo $user->email ?></option>
    <?php } ?>
</select>