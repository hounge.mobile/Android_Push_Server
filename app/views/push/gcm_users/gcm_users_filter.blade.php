<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <!--<th>ID</th>-->                        
            <th>Name</th>
            <th>Email</th>
            <th>Register ID</th>
            <th>Registeration date</th>
            <th style="width: 150px">Is active</th>
            <th style="width: 260px">Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($users as $user) { ?>
            <tr>
                <!--<td><?php echo $user->id ?></td>-->                        
                <td><?php echo $user->name ?></td>
                <td><?php echo $user->email ?></td>

                <td><a id='regIDA' data-toggle="popover" data-placement="right" data-content="<?php echo $user->gcm_regid ?>" ><?php echo substr($user->gcm_regid, 0, 4) ?>.....<?php echo substr($user->gcm_regid, -5, -1) ?></a></td>
                <td><?php echo $user->created_at ?></td>
                <td>
                    <div class="switch switch-blue">
                        <input type="radio" onchange="javascript:return fn_activate('<?php echo $user->id ?>', 1);" class="switch-input" name="isactive<?php echo $user->id ?>" value="1" id="active<?php echo $user->id ?>" <?php echo $user->activated == 1 ? 'checked' : '' ?> >
                        <label for="active<?php echo $user->id ?>" class="switch-label switch-label-off">ON</label>
                        <input type="radio" onchange="javascript:return fn_activate('<?php echo $user->id ?>', 0);" class="switch-input" name="isactive<?php echo $user->id ?>" value="0" id="notactive<?php echo $user->id ?>" <?php echo $user->activated == 0 ? 'checked' : '' ?>>
                        <label for="notactive<?php echo $user->id ?>" class="switch-label switch-label-on">OFF</label>
                        <span class="switch-selection"></span>
                    </div>
                    <div id="loadingEn<?php echo $user->id ?>">

                    </div>
                </td>
                <td class='action'>
                    <a class="btn btn-success <?php echo $user->activated == 1 ? '' : 'disabled' ?>" href="<?php echo URL::to('send-notif/send/' . $user->id) ?>" title='Send' >
                        <i class='icon-cloud'></i>
                    </a>

                    <a class="btn btn-info" href="<?php echo URL::to('gcm-users/edit/' . $user->id) ?>" title="Edit">
                        <i class='icon-edit'></i>
                    </a>

                    <button class="btn btn-danger" title="Delete" data-toggle="modal" data-target="#deleteModal<?php echo $user->id ?>">
                        <i class='icon-trash'></i>
                    </button>
                    <div class="modal fade" id="deleteModal<?php echo $user->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog" >
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete user</h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo Form::open(array('id' => 'deleteForm' . $user->id, 'url' => URL::to('gcm-users/delete/' . $user->id))) ?>
                                    Are you sure you want to delete user <span style="font-weight: bold;font-size: 15px"> <?php echo $user->name ?></span>                               
                                </div>

                                <div class="modal-footer">
                                    <button type="button" onclick="deleteUser(<?php echo $user->id ?>)" id="deleteButton<?php echo $user->id ?>" class="btn btn-danger">Delete</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>                                
                                </div>

                                <?php echo Form::close() ?>
                            </div>
                        </div>
                    </div>

                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php echo $users->links() ?>