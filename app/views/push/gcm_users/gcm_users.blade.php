@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb2' >
        <span class='title' style="padding-right: 15px;" >Users</span>
        <a style="color: #fff;" class="btn btn-success" href="<?php  echo URL::to('gcm-users/add/') ?>">New</a> 
<!--        <li class='title'>Users</li>
        <li>
            <div class="btn-group" >
                <a style="color: #fff;height: 30px;padding-top: 5px;padding-bottom: 5px" class="btn btn-success" href="<?php  echo URL::to('gcm-users/add/') ?>">New</a> 
            </div>
        </li>-->
    </ul>
</section>
<div id='content'>
    <?php $msg = Session::get('msg'); ?>
    <div id = "msg1" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg col-md-6 alert alert-info alert-dismissable">            
        <button type="button" class="close"  aria-hidden="true">×</button>
        <span><?php  echo isset($msg) ? $msg : '' ?></span>
    </div>
    <div id="loadingDel">
    </div>
    <div class="clearfix"></div>
    <div class="panel panel-default grid">
        <div class='panel-body filters'>
            <div class='col-md-12'>
                <?php  echo  Form::open(array('url' => URL::to('setting/edit'),'id'=>"editSettingForm"))  ?>
                <div class="form-inline">
                    <div class="form-group">
                        <?php  echo Form::label('searchValue', 'Search:') ?>
                        <?php  echo Form::text('searchValue','',array('class'=>'form-control','placeholder'=>'Search','id' =>'searchValue','onkeyup'=>'searchUsers(1)')) ?>                               
                    </div>
                </div>
                <?php  echo  Form::close()  ?>
            </div>
        </div>
        <div id='GcmUsers'>
            @include('push.gcm_users.gcm_users_filter')
        </div>
    </div>


</div>
<script type = "text/javascript">
    $("#msg").hide();
    var url = "Processing... <img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>>";

    $('.close').click(function() {

        $('#msg1').hide();

    })

    function deleteUser(id)
    {
        $('#loadingDel').html(url);
        $('#deleteModal' + id).modal('hide');
//        $("#deleteForm" + id).submit();
//        $('#loadingDel').html("");
        $.post("<?php  echo URL::to('gcm-users/delete') ?>", {id: id},
        function(data) {
            var ret = JSON.parse(data);
            var flag = ret[0];
            var msg = ret[1];

            $("#msg1").removeClass();

            $('#loadingDel').html("");
            $("#msg1").show();

            if (flag)
            {
                $("#msg1").addClass("msg col-md-6 alert alert-info alert-dismissable");

            } else
            {
                $("#msg1").addClass("msg col-md-6 alert alert-danger alert-dismissable");
            }
            $("#msg1 span").html(msg);
            var page = window.location.href.split('page=')[1];
            if (page == Number.NaN || page <= 0) {
                searchUsers(1);
            } else {
                searchUsers(page);
            }
        });
    }
    function fn_activate(user_id, active)
    {
        // active :: 1 active , 0 not active
        $('#loadingEn' + user_id).html(url);
        $.post("<?php  echo URL::to('gcm-users/activate') ?>", {id: user_id, active: active},
        function(data) {
            $('#loadingEn' + user_id).html("");
        });
    }
    /*---------------------search-----------------*/
    $(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('#', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            } else {
                searchReports(page);
            }
        }
    });
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(e) {
            searchReports($(this).attr('href').split('page=')[1]);
            e.preventDefault();
        });
    });
    function searchUsers(page)
    {
        var searchValue = $("#searchValue").val();
        $.get("?page=" + page, {searchValue: searchValue, dataType: 'json'},
        function(data) {
            $("#GcmUsers").html(data);
            location.hash = page;
        });
    }
    $(function() {
        $('[data-toggle="popover"]').popover({trigger: "hover"});
    });
</script>
@stop