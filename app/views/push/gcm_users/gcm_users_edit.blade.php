@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Edit user</li>
    </ul>
</section>
<div id='content'>   
    <div class="col-lg-6">      
        <?php
        $msg = Session::get('msg');
        $state = Session::get('state');
        ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg alert alert-dismissable alert-<?php  echo isset($msg) ? $state==1 ? 'info' : 'danger' : 'danger' ?>">            
            <button type="button" class="close"  aria-hidden="true">×</button>
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <?php  echo  Form::open(array('id'=>"editUserForm",'url' => URL::to('gcm-users/edit')))  ?>
        <div class="form-group">
            <?php  echo Form::hidden('id', $user->id) ?>
            <?php  echo Form::label('gcm_regid', 'Register ID') ?>
            <?php  echo Form::text('gcm_regid',$user->gcm_regid,array('class'=>'form-control','placeholder'=>'Register ID')) ?>                               
            <p class="alert-danger"><?php  echo $errors->first('gcm_regid') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::label('name', 'User name') ?>
            <?php  echo Form::text('name',$user->name,array('class'=>'form-control','placeholder'=>'User name')) ?>                               
            <p class="alert-danger"><?php  echo $errors->first('name') ?></p>
        </div>
        <div class="form-group">
            <?php  echo Form::label('email', 'Email') ?>
            <?php  echo Form::text('email',$user->email,array('class'=>'form-control','placeholder'=>'Email')) ?>                               
            <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
        </div>
        <?php  echo Form::submit('Save',array('class'=>'btn btn-primary','id'=>'addUser')) ?> 
        <?php  echo  Form::close()  ?>
    </div> 
</div>
<script type = "text/javascript">
    var urlLoading = "<img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>> Processing...";

    $("#editUserForm").validate({
        rules: {
            gcm_regid: {
                required: true,
                minlength: 10
            }
            ,
            name: {
                required: true,
                minlength: 4
            },
            email: {
                required: true,
                email: true
            }
        },
        messages: {
            gcm_regid: {
                required: "Please enter register ID",
                minlength: "Register ID must be at least 10 characters"
            }
            ,
            name: {
                required: "Please enter user name",
                minlength: "User name at least 4 characters"
            },
            email: {
                required: "Please enter email",
                minlength: "Please enter valid email"
            }
        }
    });
    $('.close').click(function() {
        $('#msg').hide();
    })
</script>
@stop