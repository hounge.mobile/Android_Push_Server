@extends('layout.master-no-sidebar')

@section('content')

<div class='row'>
    <div class='col-lg-12'>
        <?php $msg = Session::get('msg'); ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg span7 alert alert-danger">            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <?php  echo  Form::open(array('url' => '/login/login','id'=>'LoginForm'))  ?>
        <fieldset class='text-center'>
            <legend>Login to your account</legend>
            <div class="form-group">
                <?php  echo Form::text('email','',array('class'=>'form-control','placeholder'=>'E-mail')) ?>                               
                <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
            </div>
            <div class="form-group">
                <input type="password" id="password" placeholder="Password" name="password" class="form-control" />
                <p class="alert-danger"><?php  echo $errors->first('password') ?></p>
            </div>
            <div class='text-center'>
                <div class='checkbox'>
                    <label>
                        <input type='checkbox'>
                        Remember me on this computer
                    </label>
                </div>
                <?php  echo Form::submit('Sign in',array('class'=>'btn btn-default')) ?>
                <br>
                <a href="<?php  echo URL::to('user/reset-password') ?>" class="btn-link" >Forgot password?</a>
            </div>

        </fieldset>
        <?php  echo  Form::close()  ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#LoginForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                }
            },
            messages: {
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password is at least 6 chars"
                }
            }
        });
    });
</script>
@stop