@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb2'>
        <span class='title' style="padding-right: 15px;" >Notification types</span>
        <a style="color: #fff;" class="btn btn-success" href="<?php  echo URL::to('message-types/add-msg-type/') ?>">New</a> 
<!--        <li class='title'>Notification types</li>
        <li>
            <div class="btn-group" >
                <a style="color: #fff;height: 30px;padding-top: 5px;padding-bottom: 5px" class="btn btn-success" href="<?php  echo URL::to('message-types/add-msg-type/') ?>">New</a> 
            </div>
        </li>-->
    </ul>
</section>
<div id='content'>
    <?php $msg = Session::get('msg'); ?>
    <div id = "msg1" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg col-md-6 alert alert-info alert-dismissable">            
        <button type="button" class="close" aria-hidden="true">×</button>
        <span><?php  echo isset($msg) ? $msg : '' ?></span>
    </div>
    <div id="loadingDel">
    </div>
    <div class="clearfix"></div>
    <div class="panel panel-default grid">
        <div id='messageTypes'>
            @include('push.message.message_type_filter')
        </div>
    </div>

</div>
<script type = "text/javascript">

    var urlDelMsgType = "<?php  echo URL::to('message-types/del-msg-type') ?>";
    var url = "Processing... <img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>>";

    $('.close').click(function() {

        $('#msg1').hide();

    })

    function deleteMessageType(id)
    {
//        alert(id)
        $('#loadingDel').html(url);
        $('#deleteModal' + id).modal('hide');
//        $("#deleteForm" + id).submit();
//        $('#loadingDel').html("");
//        $('#loading' + id).html(url);
        $.post(urlDelMsgType, {id: id},
        function(data) {
            var ret = JSON.parse(data);
            var flag = ret[0];
            var msg = ret[1];

            $("#msg1").removeClass();
            $('#loadingDel').html("");
            $("#msg1").show();

            if (flag)
            {
                $("#msg1").addClass("msg col-md-6 alert alert-info alert-dismissable");

            } else
            {
                $("#msg1").addClass("msg col-md-6 alert alert-danger alert-dismissable");
            }

            $("#msg1 span").html(msg);

            var page = window.location.href.split('page=')[1];
            if (page == Number.NaN || page <= 0) {
                searchTypes(1);
            } else {
                searchTypes(page);
            }

        });
        function searchTypes(page)
        {
            $.get("?page=" + page, {dataType: 'json'},
            function(data) {
                $("#messageTypes").html(data);
                location.hash = page;
            });
        }
    }

</script>
@stop