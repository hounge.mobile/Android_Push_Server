<table class="table table-hover table-bordered">
    <thead>
        <tr>
            <th>Type</th>
            <th>Create date</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($types as $type) { ?>
            <tr>                      
                <td><?php echo $type->type_name ?></td>
                <td><?php echo $type->created_at ?></td>   
                <td class='action'>
                    <a class="btn btn-info" href="<?php echo URL::to('message-types/edit-msg-type/' . $type->id) ?>" title="Edit">
                        <i class='icon-edit'></i>
                    </a>

                    <button class="btn btn-danger" data-toggle="modal" data-target="#deleteModal<?php echo $type->id ?>" title="Delete">
                        <i class='icon-trash'></i>
                    </button>
                    <div class="modal fade" id="deleteModal<?php echo $type->id ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h4 class="modal-title" id="myModalLabel">Delete notification type</h4>
                                </div>
                                <div class="modal-body">
                                    <?php echo Form::open(array('id' => 'deleteForm' . $type->id, 'url' => URL::to('message-types/del-msg-type/' . $type->id))) ?>
                                    Are you sure you want to delete notification type <span style="font-weight: bold;font-size: 15px"><?php echo $type->type_name ?></span>
                                </div>

                                <div class="modal-footer">                                    
                                    <button type="button" onclick="deleteMessageType(<?php echo $type->id ?>)" id="deleteButton<?php echo $type->id ?>" class="btn btn-danger">Delete</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                </div>

                                <?php echo Form::close() ?>
                            </div>
                        </div>
                    </div>
                </td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<?php echo $types->links() ?>