@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Edit notification type</li>
    </ul>
</section>
<div id='content'>  
    <div class="col-md-12" style="padding: 0px"> 
        <div class="col-md-7"> 
            <div class="form-group">
                <div id = "msg2" class = "msg alert alert-danger alert-dismissable" style="display: none" >
                    <button type="button" class="close" data-hide="alert" aria-hidden="true">×</button>
                    <span></span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="padding: 0px" id="MessageEdit">    
        @include('push.message.edit')
    </div> 
</div>
<script type = "text/javascript">

    var urlMsgTypes = "<?php  echo URL::to('message-types/') ?>";
    var urlGetJson = "<?php  echo URL::to('message-types/get-json') ?>";

    var urlEditMsgType = "<?php  echo URL::to('message-types/edit-msg-type') ?>";
    var urlPostEditMsgType = "<?php  echo URL::to('message-types/edit-msg-type') ?>";

    var urlDelMsgTypeCol = "<?php  echo URL::to('message-types/del-msg-type-column') ?>";


    var urlLoading = "<img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>> Processing...";

</script>
<script src="<?php  echo asset('backend/js/Forms/AddMsgType.js') ?>"></script>
<script type = "text/javascript">
    getJSon();
</script>
@stop