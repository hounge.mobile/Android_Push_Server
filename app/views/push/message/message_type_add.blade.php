@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Add notification type</li>
    </ul>
</section>
<div id='content'> 
    <div class="col-md-12" style="padding: 0px">         
        <div class="col-md-7">   
            <div class="form-group">
                <div id = "msg2" class = "msg alert alert-danger alert-dismissable" style="display: none" >
                     <button type="button" class="close" data-hide="alert" aria-hidden="true">×</button>
                    <span></span>
                </div>
            </div>
            <?php  echo  Form::open(array('id'=>"addMsgTypeForm"))  ?>
            <div class="form-group">
                <?php  echo Form::label('type_name', 'Notification type name') ?>
                <input type="text" class="form-control" id='type_name' name="type_name" placeholder="Notification type name" required title="Please enter type name" />
                <p class="alert-danger"><?php  echo $errors->first('type_name') ?></p>
            </div>
            <div id = "msg" class = "msg" style="color: red">

            </div>
            <!--<?php  echo Form::submit('addCol',array('class'=>'btn btn-primary')) ?>-->
            <!--<div class="clearfix"></div>-->
        </div>
        <div class="col-md-7"> 
            <div class="panel panel-warning">
                <div class="panel-heading">
                    Fileds
                </div>
                <div class="panel-body">


                    <table class="table table-hover table-bordered" id='MsgTypeColsTable' >
                        <thead>
                            <tr>
                                <th style="width: 35px"></th>
                                <th>Key</th>
                                <th>Value</th>
                                <th style="width: 85px" ></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <p>1</p>
                                    <input type="hidden" id="rowNo1" name = rows[] value = "1"  >
                                    <input type="hidden" id="rowNoDB1" name = rowsDB[] value = "">
                                </td>
                                <td><input type="text" id='key1'   name="key[]" onkeyup="getJSon()"  title="Please enter key 1" class="form-control"/></td>
                                <td><input type="text" id='value1' name = "value[]" onkeyup="getJSon()"  title="Please enter value 1" class="form-control"/></td>
                                <td>
                                    <button type="button" class="btn btn-primary" onClick="addRow(1)" >+</button>
                                    <button type="button" class="btn btn-primary" onClick="deleteRow(1)" >-</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="col-md-4 pull-left" style="padding: 0px" >
                <?php  echo Form::button('Save message type',array('class'=>'btn btn-primary','id'=>'addMsgType')) ?>                    
            </div>
            <div class="col-md-4 pull-left">
                <div id="loading">

                </div>
            </div>
            <?php  echo  Form::close()  ?>
        </div>
        <div class="col-md-5">  
            <div class="panel panel-warning">
                <div class="panel-heading">
                    Json preview
                </div>
                <div class="panel-body">
                    <div id='jsonPreview'>

                    </div>
                </div>
            </div>
        </div>
    </div> 
</div>
<script type = "text/javascript">

    var urlMsgTypes = "<?php  echo URL::to('message-types/') ?>";
    var urlGetJson = "<?php  echo URL::to('message-types/get-json') ?>";
    var urlAddMsgType = "<?php  echo URL::to('message-types/add-msg-type') ?>";
    var urlLoading = "<img src=<?php  echo  URL::asset('backend/images/Chasing_blocks.gif')  ?>> Processing...";

</script>
<script src="<?php  echo asset('backend/js/Forms/AddMsgType.js') ?>"></script>
@stop