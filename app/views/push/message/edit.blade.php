<div class="col-md-7"> 
    <?php echo Form::open(array('id' => "editMsgTypeForm")) ?>
    <div class="form-group">
        <?php echo Form::label('type_name', 'Notification type name') ?>
        <input type="text" class="form-control" value="<?php echo $type->type_name ?>" id='type_name' name="type_name" placeholder="Notification type name" required title="Please enter type name" />
        <input type="hidden" value="<?php echo $type->id ?>" id="type_id" name="type_id" />
        <p class="alert-danger"><?php echo $errors->first('type_name') ?></p>
    </div>
    <div id = "msg" class = "msg" style="color: red">

    </div>
    <!--<?php echo Form::submit('editCol', array('class' => 'btn btn-primary')) ?>-->
</div>
<div class="col-md-7"> 
    <div class="panel panel-warning">
        <div class="panel-heading">
            Fileds
        </div>
        <div class="panel-body">


            <table class="table table-hover table-bordered" id='MsgTypeColsTable' >
                <thead>
                    <tr>
                        <th style="width: 35px"></th>
                        <th>Key</th>
                        <th>Value</th>
                        <th style="width: 85px" ></th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i = 1 ?>
                    <?php foreach ($columns as $column) { ?>
                        <tr>
                            <td>
                                <p><?php echo $i ?></p>
                                <input type="hidden" id="rowNo<?php echo $i ?>" name = rows[] value = "<?php echo $i ?>">
                                <input type="hidden" id="rowNoDB<?php echo $i ?>" name = rowsDB[] value = "<?php echo $column->id ?>">
                            </td>
                            <td><input type="text" id='key<?php echo $i ?>' value="<?php echo $column->key ?>"   name="key[]" onkeyup="getJSon()"  title="Please enter key <?php echo $column->id ?>" class="form-control"/></td>
                            <td><input type="text" id='value<?php echo $i ?>' value="<?php echo $column->value ?>"  name = "value[]" onkeyup="getJSon()"  title="Please enter value <?php echo $column->id ?>" class="form-control"/></td>
                            <td>
                                <button type="button" class="btn btn-primary" onClick="addRow(<?php echo $i ?>)" >+</button>
                                <button type="button" class="btn btn-primary" onClick="deleteRow(<?php echo $i ?>)" >-</button>
                            </td>
                        </tr>
                        <?php $i++ ?>
                    <?php } ?>

                </tbody>
            </table>
        </div>
    </div>

    <div class="col-md-4 pull-left" style="padding: 0px" >
        <?php echo Form::button('Save message type', array('class' => 'btn btn-primary', 'id' => 'editMsgType')) ?>               
    </div>
    <div class="col-md-4 pull-left">
        <div id="loading">

        </div>
    </div>

    <?php echo Form::close() ?>
</div>
<div class="col-md-5">  
    <div class="panel panel-warning">
        <div class="panel-heading">
            Json preview
        </div>
        <div class="panel-body">
            <div id='jsonPreview'>

            </div>
        </div>
    </div>
</div>