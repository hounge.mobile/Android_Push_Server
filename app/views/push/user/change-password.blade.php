@extends('layout.master-no-sidebar')

@section('content')
<div class='row'>
    <div class='col-lg-12'>
        <?php $msg = Session::get('msg'); ?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg span7 alert alert-danger">            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <?php  echo  Form::open(array('url' => URL::to('user/reset'),'id'=>'ChangePassForm'))  ?>
        <fieldset class='text-center'>
            <legend>Change password</legend>
            <?php  echo Form::hidden('id',$user->id,array('id'=>'id')) ?>                               

            <?php  echo Form::hidden('reset_code',$rest_code,array('id'=>'reset_code')) ?>                               
            <div class="form-group">
                <!--<?php  echo Form::label('email', 'E-mail') ?>-->
                <?php  echo Form::text('email',$user->email,array('class'=>'form-control','placeholder'=>'E-mail','readonly'=>'true')) ?>                               
                <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
            </div>
            <div class="form-group">
                <!--<?php  echo Form::label('password', 'New password') ?>--> 
                <input type="password" id="password" name="password" class="form-control" placeholder="Password" />
                <p class="alert-danger"><?php  echo $errors->first('password') ?></p>
            </div>
            <div class="form-group">
                <!--<?php  echo Form::label('confirm_password', 'Confirm password') ?>-->                              
                <input type="password" id="password_confirmation" name="password_confirmation" placeholder="Confirm password" class="form-control" />
                <p class="alert-danger"><?php  echo $errors->first('password_confirmation') ?></p>
            </div>
            <?php  echo Form::submit('Change password',array('class'=>'btn btn-default')) ?>
        </fieldset>
        <?php  echo  Form::close()  ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {

        $("#ChangePassForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                },
                password: {
                    required: true,
                    minlength: 6
                },
                password_confirmation: {
                    required: true,
                    minlength: 6,
                    equalTo: "#password"
                }
            },
            messages: {
                email: {
                    required: 'Please eEnter email',
                    email: 'Please enter valid email'
                },
                password: {
                    required: "Please enter password",
                    minlength: "Password is at least 6 chars"
                },
                password_confirmation: {
                    required: 'Please confirm password',
                    minlength: 'Confirm password must be at least 6 characters',
                    equalTo: 'Confirm password didnt match password'
                }
            }
        });
    });
</script>
@stop