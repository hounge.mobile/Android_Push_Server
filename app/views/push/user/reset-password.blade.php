@extends('layout.master-no-sidebar')

@section('content')
<div class='row'>
    <div class='col-lg-12'>
        <?php 
        $msg = Session::get('msg'); 
        $state = Session::get('state');?>
        <div id = "msg" style="<?php  echo isset($msg) ? '' : 'display:none' ?>" class = "msg alert alert-<?php  echo isset($msg) ? $state==1 ? 'info' : 'danger' : 'danger' ?>">            
            
            <?php  echo isset($msg) ? $msg : '' ?>
        </div>
        <?php  echo  Form::open(array('url' => '/user/reset-password','id'=>'resetForm'))  ?>
        <fieldset class='text-center'>
            <legend>Reset Password</legend>
            <p>Enter your email address below and we will send you a reset password link.</p>
            <div class="form-group">
                <?php  echo Form::text('email','',array('class'=>'form-control','placeholder'=>'E-mail')) ?>                               
                <p class="alert-danger"><?php  echo $errors->first('email') ?></p>
            </div>
            <?php  echo Form::submit('Submit',array('class'=>'btn btn-default')) ?>
        </fieldset>
        <?php  echo  Form::close()  ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $("#resetForm").validate({
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Please enter email",
                    email: "Please enter valid email"
                }
            }
        });
    });
</script>
@stop