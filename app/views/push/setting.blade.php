@extends('layout.master')

@section('content')
<section id='tools'>
    <ul class='breadcrumb' id='breadcrumb'>
        <li class='title'>Settings</li>
    </ul>
</section>
<?php
$msg = Session::get('msg');
$state = Session::get('state');
?>
<div id='content'>
    <div id = "msg" style="<?php echo isset($msg) ? '' : 'display:none' ?>" class = "msg col-lg-6 alert alert-dismissable alert-<?php echo isset($msg) ? $state == 1 ? 'info' : 'danger' : 'danger' ?>">            
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <?php echo isset($msg) ? $msg : '' ?>
    </div>
    <div class="clearfix"></div>
    <div class="col-lg-6" style="padding-left: 0px;" >               
        <?php echo Form::open(array('url' => URL::to('setting/edit'), 'id' => "editSettingForm", 'files' => true)) ?>
        <div class="form-group">
            <?php
            if ($setting) {
                echo Form::hidden('id', $setting->id);
            }
            ?>
            <?php echo Form::label('api_key', 'API key') ?>
            <?php echo Form::text('api_key', $setting ? $setting->api_key : '', array('class' => 'form-control', 'placeholder' => 'API key')) ?>
            <p class="alert-danger"><?php echo $errors->first('api_key') ?></p>
        </div>
        <!--<hr/>-->
        <div class="form-group">
            <?php echo Form::label('site_name', 'Site name') ?>
            <?php echo Form::text('site_name', $setting ? $setting->site_name : '', array('class' => 'form-control', 'placeholder' => 'Site name')) ?>
            <p class="alert-danger"><?php echo $errors->first('site_name') ?></p>
        </div>
        <div class="form-group">
            <?php echo Form::label('Logo') ?>
            <?php echo Form::hidden('logo_name', $setting ? $setting->logo : '', array('id' => 'logo_name')) ?>
            <br/>
            <img src="<?php echo asset('backend/images/' . (isset($setting->logo) && (!empty($setting->logo)) ? $setting->logo : 'default.png')) ?>" style="width: 115px;height: 50px" />
            <br/>
            <br/>
            <?php echo Form::file('logo') ?>
            <p class="alert-danger"><?php echo $errors->first('logo') ?></p>
        </div>

        <?php echo Form::submit('Save', array('class' => 'btn btn-primary')) ?>
        <?php echo Form::close() ?>
    </div>      
</div>
<script type="text/javascript" >

    $("#editSettingForm").validate({
        rules: {
            api_key: {
                required: true,
                minlength: 3
            }
        },
        messages: {
            api_key: {
                required: "Please enter API key",
                minlength: "API key must be at least 3 characters"
            }
        }
    });
</script>
@stop