<?php

/*
  |--------------------------------------------------------------------------
  | Application & Route Filters
  |--------------------------------------------------------------------------
  |
  | Below you will find the "before" and "after" events for the application
  | which may be used to do any work before or after a request into your
  | application. Here you may also register your custom route filters.
  |
 */

App::before(function($request) {
    $url = base_path() . "/installs/config.ini";

    if (!file_exists($url)) {
        try {
            fopen($url, 'w');
        } catch (Exception $ex) {
            dd($ex->getMessage());
        }
    }

    $config = parse_ini_file($url, true);

    if (isset($config['database'])) {
        $database = $config['database'];
        if (isset($database['host']) && isset($database['user']) && isset($database['password']) && isset($database['dbname'])) {
            $host = $database['host'];
            $username = $database['user'];
            $password = $database['password'];
            $dbname = $database['dbname'];

            Config::set('database.connections.mysql', array(
                'driver' => 'mysql',
                'host' => 'localhost',
                'database' => $dbname,
                'username' => $username,
                'password' => $password,
                'charset' => 'utf8',
                'collation' => 'utf8_unicode_ci',
                'prefix' => '',
            ));
        }
    }
});
Route::filter('databaseFile', function() {

    $url = base_path() . "/installs/config.ini";
    if (!file_exists($url)) {
        try {
            fopen($url, 'w');
        } catch (Exception $ex) {
            dd($ex->getMessage());
        }
    }
});

App::after(function($request, $response) {
    //
});

/*
  |--------------------------------------------------------------------------
  | Authentication Filters
  |--------------------------------------------------------------------------
  |
  | The following filters are used to verify that the user of the current
  | session is logged into this application. The "basic" filter easily
  | integrates HTTP Basic authentication for quick, simple checking.
  |
 */

Route::filter('auth', function() {
    if (Auth::guest()) {
        if (Request::ajax()) {
            return Response::make('Unauthorized', 401);
        } else {
            return Redirect::guest('login');
        }
    }
});


Route::filter('auth.basic', function() {
    return Auth::basic();
});

/*
  |--------------------------------------------------------------------------
  | Guest Filter
  |--------------------------------------------------------------------------
  |
  | The "guest" filter is the counterpart of the authentication filters as
  | it simply checks that the current user is not logged in. A redirect
  | response will be issued if they are, which you may freely change.
  |
 */

Route::filter('guest', function() {
    if (Auth::check())
        return Redirect::to('/');
});

/*
  |--------------------------------------------------------------------------
  | CSRF Protection Filter
  |--------------------------------------------------------------------------
  |
  | The CSRF filter is responsible for protecting your application against
  | cross-site request forgery attacks. If this special token in a user
  | session does not match the one given in this request, we'll bail.
  |
 */

Route::filter('csrf', function() {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});
Route::filter('sentry.auth', function() {
    if (!Sentry::check()) { //kda lw el filter mesh passed , hy3ml redirect 3la el login page
        return Redirect::to('login');
    }
});
View::composer('layout.master', function($view) {
    $setting = Setting::first();
    $view->with('setting_share', $setting);
});

Route::filter('track', function() {

    $route = Route::getCurrentRoute()->getPath();

    $track_obj = new Track();
    $track = $track_obj->getTrack();

    if ($track == 0) {
        return $route == 'install' ? View::make('install.database') : Redirect::to('install');
    } elseif ($track == 1) {
        return strpos($route, 'install/admin') !== false && $route != 'install' ? View::make('install.admin_account') : Redirect::to('install/admin');
    } elseif ($track == 2) {
        if (strpos($route, 'install') !== false) {
            return Redirect::to('login');
        }
    }
});
App::error(function(PDOException $exception) {
    Log::error("Error connecting to database: " . $exception->getMessage());

    return "Error connecting to database";
});
