<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushCreatePushMessagesTable extends Migration {

    public function up() {
        Schema::create('gcm_messages', function($table) {
            $table->increments('id');
            $table->text('message');
            $table->text('link');
            $table->text('image');
            $table->integer('sent_flag')->default(0);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_messages');
    }

}
