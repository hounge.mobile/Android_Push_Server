<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGcmSettingsTable2 extends Migration {

    public function up() {
        Schema::table('gcm_settings', function($table) {
            $table->string('site_name');
        });
    }

    public function down() {
        Schema::table('gcm_settings', function($table) {
            $table->dropColumn('site_name');
        });
    }

}
