<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGcmStatus extends Migration {

    public function up() {
        Schema::create('gcm_status', function($table) {
            $table->increments('id');
            $table->string('name');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_status');
    }

}
