<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGcmSettingsTable extends Migration {

    public function up() {
         Schema::table('gcm_settings', function($table) {
             $table->string('logo');
        });
    }

    public function down() {
        Schema::table('gcm_settings', function($table) {
            $table->dropColumn('logo');
        });
    }

}
