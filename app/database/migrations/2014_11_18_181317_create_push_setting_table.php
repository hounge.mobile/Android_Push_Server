<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePushSettingTable extends Migration {

    public function up() {
        Schema::create('gcm_settings', function($table) {
            $table->increments('id');
            $table->longText('api_key');

            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_settings');
    }

}
