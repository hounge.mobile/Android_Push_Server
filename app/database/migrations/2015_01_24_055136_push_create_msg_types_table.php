<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushCreateMsgTypesTable extends Migration {

    public function up() {
        Schema::create('gcm_message_types', function($table) {
            $table->increments('id');
            $table->string('type_name');

            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_message_types');
    }

}
