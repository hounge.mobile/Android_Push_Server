<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterGcmMsgTypesTable2 extends Migration {

    public function up() {
        Schema::table('gcm_message_types', function($table) {
            $table->unique('type_name');
        });
    }

    public function down() {
        Schema::table('gcm_message_types', function($table) {
            $table->unique('type_name');
        });
    }

}
