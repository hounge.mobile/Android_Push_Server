<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushCreateGcmUsersTable extends Migration {

    public function up() {
        Schema::create('gcm_users', function($table) {
            $table->increments('id');
            $table->string('gcm_regid');
            $table->string('name', 255);
            $table->string('email', 255);
            $table->tinyInteger('activated')->default(0);
            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_users');
    }

}
