<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableGcmMessagesTable extends Migration {

    public function up() {
        Schema::table('gcm_messages', function($table) {
            $table->integer('type_id');
        });
    }

    public function down() {
        Schema::table('gcm_messages', function($table) {
            $table->dropColumn('type_id');
        });
    }

}
