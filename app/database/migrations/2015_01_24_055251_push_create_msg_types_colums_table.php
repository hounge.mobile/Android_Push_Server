<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushCreateMsgTypesColumsTable extends Migration {

    public function up() {
        Schema::create('gcm_message_types_colums', function($table) {
            $table->increments('id');
            $table->integer('type_id')->unsigned();

            $table->string('key');
            $table->string('value');

            $table->unique(array('type_id', 'key'));

            $table->timestamp('created_at');
            $table->timestamp('updated_at');
            
//            $table->foreign('type_id')->references('id')->on('gcm_message_types');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_message_types_colums');
    }

}
