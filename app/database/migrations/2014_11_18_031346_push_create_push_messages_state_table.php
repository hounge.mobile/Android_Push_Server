<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PushCreatePushMessagesStateTable extends Migration {

    public function up() {
        Schema::create('gcm_messages_state', function($table) {

            $table->increments('id');

            $table->integer('message_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->integer('state')->default(0);
            $table->string('state_text');

            $table->timestamp('created_at');
            $table->timestamp('updated_at');
        });
    }

    public function down() {
        Schema::dropIfExists('gcm_messages_state');
    }

}
